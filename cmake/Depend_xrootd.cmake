
include(Tools)

find_library(SHIFT_LIBRARIES shift)
find_path(SHIFT_INCLUDE_DIRS Cns.h PATH_SUFFIXES shift)
find_library(CASTORCOMMON_LIBRARIES castorcommon)
find_library(CASTORNS_LIBRARIES castorns)
find_library(CASTORCLIENT_LIBRARIES castorclient)

assert(SHIFT_LIBRARIES AND SHIFT_INCLUDE_DIRS
    MESSAGE "Failed to find libshift, please install castor-devel package")
list(APPEND SHIFT_LIBRARIES
${CASTORCOMMON_LIBRARIES} ${CASTORNS_LIBRARIES} ${CASTORCLIENT_LIBRARIES})

find_library(XROOTD_LIBRARIES XrdPosix)
find_library(XROOTDCL_LIBRARIES XrdCl)
find_path(XROOTD_INCLUDE_DIRS XrdPosix/XrdPosixXrootd.hh PATH_SUFFIXES xrootd)

assert(XROOTD_LIBRARIES AND XROOTDCL_LIBRARIES AND XROOTD_INCLUDE_DIRS
    MESSAGE "Failed to find xrootd package")
list(APPEND XROOTD_LIBRARIES ${XROOTDCL_LIBRARIES})
