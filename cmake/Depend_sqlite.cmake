
include(Tools)
include(FindPkgConfig)

pkg_check_modules(SQLITE sqlite3)

if(NOT SQLITE_FOUND OR NOT SQLITE_LIBRARIES)
  find_path(SQLITE_INCLUDE_DIRS NAMES sqlite3.h)
  find_library(SQLITE_LIBRARIES sqlite3)
  assert(SQLITE_INCLUDE_DIRS AND SQLITE_LIBRARIES MESSAGE "sqlite3 libraries not found")
endif()
