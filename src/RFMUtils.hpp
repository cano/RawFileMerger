/*
 * RFMUtils.hpp
 *
 *  Created on: Nov 3, 2015
 *      Author: mdonze
 */

#ifndef RFMUTILS_HPP__
#define RFMUTILS_HPP__

#include <chrono>
#include <cstdint>
#include <string>

// see: https://bugzilla.redhat.com/show_bug.cgi?id=999320
#include <boost/config.hpp>
#if defined(__clang__) && __cplusplus >= 201103L
#undef BOOST_NO_CXX11_HDR_TUPLE
#endif
#include <boost/signals2.hpp>

#include "data/MergedFile.hpp"

namespace ntof {
namespace rfm {

class TransferRate
{
public:
    TransferRate();
    void update(std::size_t transferred);

    std::size_t transferred;
    double rate;

protected:
    std::chrono::time_point<std::chrono::steady_clock> m_last;
};

typedef boost::signals2::signal<void(const std::string &id,
                                     const std::string &message)>
    ErrorSignal;
typedef ErrorSignal WarningSignal;

typedef boost::signals2::signal<void(MergedFile &file,
                                     const std::string &errorMsg)>
    FileErrorSignal;

typedef boost::signals2::signal<void(const std::string &err)> TaskSignal;

} // namespace rfm
} /* namespace ntof */

#endif
