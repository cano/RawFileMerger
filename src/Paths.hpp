/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-10T11:26:11+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef PATHS_HPP__
#define PATHS_HPP__

#include <boost/filesystem.hpp>

namespace ntof {
namespace rfm {

class DaqInfo;
class RunInfo;
class MergedFile;
class FileEvent;

class Paths
{
public:
    Paths() = delete;

    /**
     * @brief get run path on EACS or DAQ when in Standalone mode
     * @details
     * if Config::isStandalone :
     *   /mnt/ntofdaq-m111.cern.ch/run12345 (= getPath(daq, run))
     * else
     *   /DAQ/data/run12345
     */
    static boost::filesystem::path getPath(const RunInfo &run);

    /**
     * @brief get run path on the given DAQ
     * @details ex: /mnt/ntofdaq-m111.cern.ch/run12345
     */
    static boost::filesystem::path getPath(const DaqInfo &daq,
                                           const RunInfo &run);

    /**
     * @brief get raw data path on the given DAQ
     * @details ex: /mnt/ntofdaq-m111.cern.ch/run12345/run12345_1_s1.raw
     */
    static boost::filesystem::path getRawFilePath(const DaqInfo &daq,
                                                  const RunInfo &run,
                                                  const FileEvent &event);

    /**
     * @brief get event file path
     * @throws RFMException on invalid standaloneDaq runs
     * @returns getPath(run) / run12345_1_s1.event
     */
    static boost::filesystem::path getEventFilePath(const RunInfo &run,
                                                    const FileEvent &event);

    /**
     * @brief get run file path
     * @throws RFMException on invalid standaloneDaq runs
     * @returns getPath(run) / run12345.run
     */
    static boost::filesystem::path getRunFilePath(const RunInfo &run);

    /**
     * @brief get remote path for a given run
     */
    static boost::filesystem::path getRemotePath(const RunInfo &run);

    /**
     * @brief get remote directory path for the given file
     * @details can also be used with MergedFileIndex
     */
    static boost::filesystem::path getRemotePath(const RunInfo &run,
                                                 const MergedFile &file);

    /**
     * @brief get remote file path
     * @details can also be used with MergedFileIndex
     */
    static boost::filesystem::path getRemoteFilePath(const RunInfo &run,
                                                     const MergedFile &file);
};

} // namespace rfm
} // namespace ntof

#endif
