/*
 * RFMUtils.cpp
 *
 *  Created on: Nov 3, 2015
 *      Author: mdonze
 */

#include "RFMUtils.hpp"

using namespace ntof::rfm;

TransferRate::TransferRate() : transferred(0), rate(0) {}

void TransferRate::update(std::size_t newTr)
{
    if (transferred == 0 || newTr < transferred)
    {
        rate = 0;
        transferred = newTr;
    }
    else
    {
        std::chrono::time_point<std::chrono::steady_clock> now =
            std::chrono::steady_clock::now();
        std::chrono::duration<double> dur = now - m_last;
        if (dur.count() != 0)
        {
            rate = (newTr - transferred) / dur.count();
            m_last = now;
        }
        transferred = newTr;
    }
}
