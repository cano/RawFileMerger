/*
 * FileDeleter.hpp
 *
 *  Created on: Nov 5, 2015
 *      Author: mdonze
 */

#ifndef FILEDELETER_H_
#define FILEDELETER_H_

#include <condition_variable>
#include <list>
#include <map>
#include <memory>
#include <mutex>
#include <thread>
#include <vector>

#include "RFMUtils.hpp"
#include "data/MergedFile.hpp"
#include "data/RunInfo.hpp"

namespace ntof {
namespace rfm {

class FileDeleter
{
public:
    FileDeleter();
    virtual ~FileDeleter();

    /**
     * @brief add a new file to be monitored
     */
    void add(MergedFile &file);

    /**
     * @brief add a new run to be monitored
     */
    void add(RunInfo &run);

    /**
     * Starts the deleter thread
     */
    void start();

    /**
     * @brief stop the deleter thread
     */
    void stop();

    /**
     * @brief set the deleter loop interval
     * @param interval loop delay in miliseconds
     */
    void setInterval(uint32_t interval);

    ErrorSignal errorSignal;
    FileErrorSignal fileErrorSignal;

protected:
    typedef std::map<std::string, MergedFile::Shared> MergedFileMap;
    typedef std::map<RunInfo::RunNumber, RunInfo::Shared> RunMap;

    void run();

    std::string uid(const MergedFile &file) const;

    void checkFiles(MergedFileMap &files, RunMap &runs);
    void checkRuns(RunMap &runs);
    void removeOldRuns();

    void removeRawFiles(const RunInfo &run, const MergedFile &file);

    void removeRunFiles(const RunInfo &run);

    std::unique_ptr<std::thread> m_thread;
    std::mutex m_lock;
    std::condition_variable m_cond;
    bool m_started;
    uint32_t m_interval;

    MergedFileMap m_files;
    RunMap m_runs;
};

} // namespace rfm
} /* namespace ntof */

#endif /* FILEDELETER_H_ */
