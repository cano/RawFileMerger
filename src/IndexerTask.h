/*
 * IndexerTask.h
 *
 *  Created on: Jan 28, 2016
 *      Author: mdonze
 */

#ifndef INDEXERTASK_H_
#define INDEXERTASK_H_

#include <cstdint>
#include <vector>

#include "Archiver.hpp"
#include "RFMManager.hpp"
#include "RFMUtils.hpp"
#include "data/MergedFileIndex.hpp"
#include "data/RunInfo.hpp"

namespace ntof {
namespace rfm {

class EventDetails;

class IndexerTask : public RFMManager::Task
{
public:
    explicit IndexerTask(MergedFileIndex &indexFile);
    virtual ~IndexerTask();
    /**
     * Runs the merging task
     */
    void run() override;

    struct EventDetails
    {
        explicit EventDetails(const FileEvent &event);

        uint32_t streamNumber;
        uint32_t fileNumber;
        uint32_t sequenceNumber;
        uint32_t trigNumber;
        uint64_t fileOffset;
    };

    /**
     * Gets CASTOR file associated with this task
     * @return
     */
    inline MergedFileIndex::Shared indexFile() const { return m_index; }

    inline RunInfo::Shared runInfo() const { return m_run; }

    TaskSignal taskSignal;

protected:
    /**
     * Appends RCTR and MODH to the CASTOR file
     * @param cFile
     */
    void appendRunHeaders(Archiver::File &file);

    /**
     * Write INDX header to CASTOR file
     * @param cFile
     */
    void appendIndexHeader(Archiver::File &file,
                           const std::vector<EventDetails> &evtDetails);

    /**
     * Write EVEH and ADDH header to CASTOR file
     * @param cFile
     */
    void appendEventHeaders(Archiver::File &file,
                            const MergedFile::EventsList &events);

    /**
     * Write data to CASTOR file and update fOffset
     * @param cFile
     * @param size
     * @param buffer
     */
    void writeData(Archiver::File &file, uint32_t size, const void *buffer);

    RunInfo::Shared m_run;
    MergedFileIndex::Shared m_index;
    uint64_t m_offset; //!< CASTOR file actual position
};

} // namespace rfm
} /* namespace ntof */

#endif /* INDEXERTASK_H_ */
