/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-27T09:10:51+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef RFMFACTORY_HPP__
#define RFMFACTORY_HPP__

#include <map>
#include <memory>
#include <mutex>
#include <vector>

#include <Singleton.hpp>
#include <Worker.hpp>

#include "RFMDatabase.hpp"
#include "RFMUtils.hpp"
#include "data/DaqInfo.hpp"
#include "data/FileStats.hpp"
#include "data/RunInfo.hpp"

namespace ntof {
namespace rfm {

/**
 * @brief File/Event/Runs factory
 * @return [description]
 */
class RFMFactory : public ntof::utils::Singleton<RFMFactory>
{
public:
    typedef ntof::utils::Worker::SharedTask SharedTask;
    typedef ntof::utils::Worker::Task Task;
    typedef RFMDatabase::RunNumberList RunNumberList;
    typedef std::vector<DaqInfo::Shared> DaqInfoList;
    typedef std::map<DaqInfo::CrateId, DaqInfo::Shared> DaqInfoMap;
    typedef boost::signals2::signal<void(const RunInfo::Shared &)> RunSignal;
    typedef boost::signals2::signal<void(RunInfo::RunNumber)> RunNumberSignal;
    typedef boost::signals2::signal<void(const MergedFile::Shared &)>
        MergedFileSignal;
    typedef boost::signals2::signal<void(const FileEvent::Shared &)>
        FileEventSignal;

    ~RFMFactory();

    /**
     * @brief create a new run object
     * @param[in]  runNumber the run number
     * @param[in]  expArea   associated experimental area
     * @param[in]  startDate the run start date
     * @param[in]  daqs      list of daqs in the run
     * @param[in]  experiment optional experiment name
     * @return a valid RunInfo pointer on success
     */
    RunInfo::Shared createRun(RunInfo::RunNumber runNumber,
                              const std::string &expArea,
                              std::time_t startDate,
                              const DaqInfo::CrateIdList &daqs,
                              const std::string &experiment = std::string());

    /**
     * @brief retrieve a run
     * @param[in]  runNumber number of the run to retrieve
     * @param[in]  load      load from db or do only a cache lookup
     * @return a valid RunInfo pointer on success
     */
    RunInfo::Shared getRun(RunInfo::RunNumber runNumber, bool load = true);

    /**
     * @brief remove a run from the database
     * @param[in]  runNumber the run to remove
     * @return false on error
     */
    bool removeRun(RunInfo::RunNumber runNumber);

    /**
     * @get all runs from the database
     * @param[out]  runs the list of runs
     * @return false on error
     */
    bool getAllRuns(RunNumberList &runs);

    /**
     * @get migrated runs from the database
     * @param[out]  runs the list of runs
     * @return false on error
     */
    bool getMigratedRuns(RunNumberList &runs);

    /**
     * @brief retrieve global file stats
     * @return false on error
     */
    bool getFileStats(FileStats &stats);

    /**
     * @brief create a file for the given run
     * @details one should rather call run->addFile() which does some
     * additional checks.
     */
    MergedFile::Shared addFile(RunInfo &run);

    /**
     * @brief create an index for the given run
     * @details one should rather call run->createIndex() which does some
     * additional checks.
     */
    MergedFileIndex::Shared createIndex(RunInfo &run);

    template<class Storable>
    SharedTask save(Storable &s);

    /**
     * @brief internal method called by run destructor
     */
    static void release(const RunInfo &info);

    /**
     * @brief retrieve info about a daq
     * @param[in]  id the daq crateId to retrieve
     */
    DaqInfo::Shared getDaqInfo(DaqInfo::CrateId id) const;

    /**
     * @brief retrieve a list of daqs
     * @param[in] list the daqs id list
     * @param[out] daqs the DaqInfo list
     * @throws RFMException if a daq is not known in the list
     */
    DaqInfoList getDaqInfoList(const RunInfo::DaqsList &list) const;

    /**
     * @brief retrieve complete list of daqs
     */
    DaqInfoList getDaqInfoList() const;

    /**
     * @brief check and load events for the given file
     * @details users should rather call MergedFile::getEvents
     */
    void loadEvents(MergedFile &file);

    /**
     * @brief add an event to the given file
     * @details users should rather call MergedFile::addEvent
     */
    FileEvent::Shared addEvent(MergedFile &file,
                               FileEvent::SequenceNumber seqNum,
                               FileEvent::EventNumber eventNumber,
                               std::size_t size);

    /**
     * @brief retrieve next retry timestamp
     * @param[out]  runNumber next retry runNumber
     * @param[out]  fileNumber next retry fileNumber
     * @param[out]  date       next retry date
     * @return true on success
     * @details may output RunInfo::InvalidRunNumber if nothing is available
     */
    bool getNextRetry(RunInfo::RunNumber &runNumber,
                      MergedFile::FileNumber &fileNumber,
                      std::time_t &date);

    /**
     * @brief get next expiry timestamp
     * @param[out]  runNumber next expiring runNumber
     * @param[out]  date       next expiring date
     * @return true on success
     */
    bool getNextExpiry(RunInfo::RunNumber &runNumber, std::time_t &date);

    RunSignal runAddSignal;
    RunSignal runUpdateSignal;
    RunNumberSignal runRemoveSignal;
    MergedFileSignal fileAddSignal;
    MergedFileSignal fileUpdateSignal;
    FileEventSignal eventAddSignal;
    FileEventSignal eventUpdateSignal;

protected:
    struct RunInfoCached
    {
        SharedTask loader;
        std::weak_ptr<RunInfo> ref;
    };

    typedef std::map<RunInfo::RunNumber, RunInfoCached> Cache;
    friend class ntof::utils::Singleton<RFMFactory>;

    explicit RFMFactory();

    inline void emitUpdate(const RunInfo::Shared &run) { runUpdateSignal(run); }
    inline void emitUpdate(const MergedFile::Shared &file)
    {
        fileUpdateSignal(file);
    }
    inline void emitUpdate(const FileEvent::Shared &e) { eventUpdateSignal(e); }

    mutable std::mutex m_lock;
    ntof::utils::Worker m_worker;
    RFMDatabase m_db;
    Cache m_cache;
    std::map<DaqInfo::CrateId, DaqInfo::Shared> m_daqs;
};

template<class Storable>
RFMFactory::SharedTask RFMFactory::save(Storable &s)
{
    std::shared_ptr<Storable> shared(s.m_weakRef.lock());

    return m_worker.post([this, shared] {
        if (!this->m_db.update(*shared))
        {
            throw this->m_db.errorString();
        }
        this->emitUpdate(shared);
    });
}

} // namespace rfm
} // namespace ntof

#endif
