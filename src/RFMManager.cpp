/*
 * RFMManager.cpp
 *
 *  Created on: Oct 8, 2015
 *      Author: mdonze
 */

#include "RFMManager.hpp"

#include <algorithm>
#include <csignal>
#include <functional>
#include <iostream>
#include <sstream>
#include <string>

#include <NTOFException.h>
#include <ctype.h>
#include <easylogging++.h>
#include <pugixml.hpp>
#include <stdio.h>
#include <sys/time.h>

#include "Config.h"
#include "FileDeleter.hpp"
#include "FileUtils.hpp"
#include "IndexerTask.h"
#include "MergerTask.h"
#include "RFMDaqListener.hpp"
#include "RFMFactory.hpp"
#include "RFMMonitoring.hpp"
#include "data/RunInfo.hpp"

using namespace ntof::rfm;

void signalHandler(int signum)
{
    LOG(DEBUG) << "Received signal #" << signum;
    signal(SIGTERM, SIG_DFL);
    signal(SIGINT, SIG_DFL);
    exit(0);
}

RFMManager::RFMManager() :
    m_worker("Merger", Config::instance().getPoolSize()),
    m_started(false),
    m_next(-1)
{
    VLOG(1) << "Constructing RFMManager object";
}

RFMManager::~RFMManager()
{
    stop();

    if (m_fileDeleter)
        m_fileDeleter->stop();

    VLOG(1) << "Destroying RFMManager object";
    m_worker.stop();
}

void RFMManager::restoreFiles()
{
    RFMFactory &factory(RFMFactory::instance());
    std::time_t now = std::time(nullptr);

    // Restores all files from DB
    RFMFactory::RunNumberList runs;
    factory.getAllRuns(runs);
    for (RunInfo::RunNumber runNumber : runs)
    {
        RunInfo::Shared run(factory.getRun(runNumber));
        bool runUpdate = false;
        bool isCopied = true;

        if (!run)
            continue;
        else if (!run->hasStopDate())
        {
            runUpdate = true;
            run->setStopDate(now);
        }

        if (run->expiryDate() == RunInfo::Expired)
        {
            runUpdate = true;
            run->setExpiryDate(now); /* expire it again */
        }

        for (MergedFile::Shared &file : run->getFiles())
        {
            switch (file->status())
            {
            case MergedFile::INCOMPLETE:
            case MergedFile::WAITING_FOR_APPROVAL:
            case MergedFile::WAITING:
            case MergedFile::TRANSFERRING:
            case MergedFile::FAILED:
                LOG(INFO) << "[RFMManager] resetting file status: "
                          << file->fileName();
                file->clearStatus();
                completeFile(*file);
                isCopied = false;
                break;
            case MergedFile::COPIED:
                // Merging was completed, deleter must check it
                // Send file to the deleter
                m_fileDeleter->add(*file);
                break;
            case MergedFile::IGNORED:
            case MergedFile::MIGRATED: break;
            default:
                LOG(ERROR) << "[RFMManager] unknown file status("
                           << file->status() << "): " << file->fileName();
                file->clearStatus();
                file->save();
                break;
            }
        }

        /* stopped runs should always have a pending index */
        MergedFileIndex::Shared index = run->createIndex();
        if (index)
        {
            switch (index->status())
            {
            case MergedFile::IGNORED:
            case MergedFile::MIGRATED: break;
            case MergedFile::COPIED:
                /* let's rely on FileDeleter to do its job */
                m_fileDeleter->add(*index);
                break;
            case MergedFile::INCOMPLETE:
                if (isCopied && run->isApproved())
                    mergeIndex(*index);
                break;
            default:
                index->clearStatus();
                if (isCopied && run->isApproved())
                    mergeIndex(*index);
                else
                    index->save();
                break;
            }
        }
        if (runUpdate)
            run->save();
        m_mon->notify(run->runNumber());
    }
}

void RFMManager::initialize()
{
    // Get Config instance
    Config &conf = Config::instance();

    m_fileDeleter.reset(new FileDeleter());
    m_cmd.reset(new RFMCommand(*this));
    m_mon.reset(new RFMMonitoring());

    m_fileDeleter->errorSignal.connect(
        [this](const std::string &ident, const std::string &msg) {
            m_mon->setError(ident, msg);
        });
    m_fileDeleter->fileErrorSignal.connect(
        [this](MergedFile &file, const std::string &msg) {
            errorOccured(file, msg);
        });

    // Reading buffer size
    if (conf.getBufferSize() > 0)
    {
        LOG(DEBUG) << "Setting buffer size to " << conf.getBufferSize() << "MB";
        MergerTask::setBufferSize(conf.getBufferSize() * 1024 * 1024);
    }

    // Install signal handler back (because Oracle steal them!)
    LOG(DEBUG) << "Installing signal handlers";
    signal(SIGTERM, signalHandler);
    signal(SIGINT, signalHandler);

    if (conf.isStandalone())
    {
        m_daqListener.reset(new RFMDaqListener(*this));
    }

    // Restores all files from DB
    restoreFiles();

    connect();
    m_mon->start();
    m_fileDeleter->start();

    // Print config
    LOG(INFO) << "=== Application settings ===";
    LOG(INFO) << "Experimental area : " << conf.getExperimentalArea();
    LOG(INFO) << "Archiver : " << Archiver::instance().name();
    LOG(INFO) << "Local path : " << conf.getPaths().local;
    LOG(INFO) << "Remote path : " << conf.getPaths().remote;
    LOG(INFO) << "Database path : " << conf.getPaths().database;
    LOG(INFO) << "Remote node : " << conf.getCastorNode() << " on SVC class "
              << conf.getSvcClass();
    LOG(INFO) << "Limits : Size : " << conf.getMinTotalSize()
              << " Max files : " << conf.getMaxEvents()
              << " Default expiry : " << conf.getDefaultExpiry();
    LOG(INFO) << "Pool size : " << conf.getPoolSize();
    LOG(INFO) << "Replace strategy : " << conf.getReplaceStrategyAsString();
    LOG(INFO) << "DIM services prefix : " << conf.getDimPrefix();
    LOG(INFO) << "Standalone mode : "
              << (conf.isStandalone() ? "active" : "inactive");
    LOG(INFO) << "DAQ mounted on : " << conf.getPaths().mount;
    LOG(INFO) << "============================";
}

void RFMManager::connect()
{
    RFMFactory &factory(RFMFactory::instance());
    factory.runAddSignal.connect([this](const RunInfo::Shared &run) {
        this->m_mon->notify(run->runNumber());
    });
    factory.runUpdateSignal.connect([this](const RunInfo::Shared &run) {
        this->m_mon->notify(run->runNumber());

        checkNextDate(run->expiryDate());
        runUpdated(run->runNumber());
    });
    factory.runRemoveSignal.connect([this](RunInfo::RunNumber runNumber) {
        this->m_mon->notify(runNumber);
    });

    factory.fileAddSignal.connect([this](const MergedFile::Shared &file) {
        this->m_mon->notify(file->runNumber(), file->fileNumber());
    });
    factory.fileUpdateSignal.connect([this](const MergedFile::Shared &file) {
        this->m_mon->notify(file->runNumber(), file->fileNumber());

        checkNextDate(file->retryDate());

        /* worth checking if file should be merged in main thread */
        if (file->status() == MergedFile::WAITING_FOR_APPROVAL)
            runUpdated(file->runNumber());
    });

    factory.eventAddSignal.connect([this](const FileEvent::Shared &event) {
        this->m_mon->notify(event->runNumber(), event->fileNumber());
    });
    factory.eventUpdateSignal.connect([this](const FileEvent::Shared &event) {
        this->m_mon->notify(event->runNumber(), event->fileNumber());
    });
}

void RFMManager::checkNextDate(std::time_t next)
{
    if (next < 0)
        return;

    std::lock_guard<std::mutex> lock(m_lock);
    if ((m_next < 0) || (next < m_next))
    {
        m_next = next;
        m_cond.notify_all();
    }
}

void RFMManager::runUpdated(RunInfo::RunNumber runNumber)
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (m_updates.insert(runNumber).second)
        m_cond.notify_all();
}

void RFMManager::setCurrentRun(RunInfo &run)
{
    if (run.isStandalone() && !m_daqListener)
    {
        m_daqListener.reset(new RFMDaqListener(*this));
        LOG(WARNING) << "[RFMManager]: standalone mode not enabled in "
                        "configuration, some events may be missing on run "
                     << run.runNumber();
    }
    m_mon->setCurrentRun(run);
}

void RFMManager::thread_func()
{
    // Starts all backgroud threads
    std::unique_lock<std::mutex> lock(m_lock);
    while (m_started)
    {
        if (m_updates.empty())
        {
            if (m_next >= 0)
            {
                std::chrono::seconds delay(m_next - std::time(nullptr));
                if (delay.count() <= 0)
                    delay = std::chrono::seconds(1);

                LOG(DEBUG)
                    << "[RFMManager] sleeping for " << delay.count() << " secs";
                // wait_until seems to be shaky (lots of spurious wakeups)
                m_cond.wait_for(lock, delay);
            }
            else
            {
                LOG(DEBUG) << "[RFMManager] sleeping forever";
                m_cond.wait(lock);
            }
        }

        std::time_t next;
        UpdatedRunsList updates;

        updates.swap(m_updates);
        next = m_next;
        m_next = std::time_t(-1);
        lock.unlock();

        LOG(TRACE)
            << "[RFMManager] loop next:" << next
            << " now:" << std::time(nullptr) << " updates:" << updates.size();

        for (RunInfo::RunNumber runNumber : updates)
        {
            /* run updates may modify: stopDate, expiryDate, experiment,
             * approval
             *
             * This is also triggered when a file is succesfuly copied
             */

            RunInfo::Shared run = RFMFactory::instance().getRun(runNumber);
            if (!run)
                continue;
            if (run->hasStopDate())
            {
                MergedFile::Shared current = run->currentFile();
                if (current && (current->status() == MergedFile::INCOMPLETE))
                    completeFile(*current);
            }
            if (run->isApproved())
            {
                bool isCopied = true;
                for (MergedFile::Shared file : run->getFiles())
                {
                    switch (file->status())
                    {
                    /* WAITING is not there to not loop-queue tasks */
                    case MergedFile::WAITING_FOR_APPROVAL:
                        completeFile(*file);
                        isCopied = false;
                        break;
                    default: isCopied = false; break;
                    case MergedFile::COPIED:
                    case MergedFile::MIGRATED:
                    case MergedFile::IGNORED: break;
                    }
                }

                if (isCopied && run->hasStopDate())
                {
                    MergedFileIndex::Shared index = run->createIndex();
                    if (!index)
                        LOG(ERROR) << "[RFMManager] failed to create index";
                    else if (index->status() != MergedFile::IGNORED)
                        mergeIndex(*index);
                    else
                        m_fileDeleter->add(*run); /* flag for completion */
                }
            }
        }

        if (next > 0)
        {
            triggerRetries();
            expireRuns();
        }

        lock.lock();
    }
}

void RFMManager::triggerRetries()
{
    /* retries */
    RFMFactory &factory = RFMFactory::instance();
    std::time_t next = std::time_t(-1);
    std::time_t now = std::time(nullptr);
    RunInfo::RunNumber runNumber;
    MergedFile::FileNumber fileNumber;

    for (factory.getNextRetry(runNumber, fileNumber, next);
         (next > 0) && (next <= now);
         factory.getNextRetry(runNumber, fileNumber, next))
    {
        RunInfo::Shared run = factory.getRun(runNumber);
        if (!run)
        {
            LOG(FATAL) << "[RFMManager] corrupted database, no "
                          "associated run for file retry";
            break;
        }

        LOG(INFO) << "[RFMManager] retry scheduled for file:" << fileNumber
                  << " in run:" << runNumber;
        if (fileNumber == MergedFile::IndexFileNumber)
        {
            MergedFileIndex::Shared index = run->index();
            if (index)
                mergeIndex(*index);
        }
        else
        {
            MergedFile::Shared file = run->findFile(fileNumber);
            if (file)
                completeFile(*file);
        }
    }

    if (next > 0)
    {
        std::lock_guard<std::mutex> lock(m_lock);
        if ((m_next < 0) || (next < m_next))
            m_next = next;
    }
}

void RFMManager::expireRuns()
{
    /* retries */
    RFMFactory &factory = RFMFactory::instance();
    std::time_t next = std::time_t(-1);
    std::time_t now = std::time(nullptr);
    RunInfo::RunNumber runNumber;

    /* expire old runs */
    for (factory.getNextExpiry(runNumber, next); (next > 0) && (next <= now);
         factory.getNextExpiry(runNumber, next))
    {
        RunInfo::Shared run = factory.getRun(runNumber);
        if (!run)
        {
            LOG(FATAL) << "[RFMManager] corrupted database, no "
                          "such expired run";
            break;
        }
        LOG(INFO) << "[RFMManager] run is expiring: " << runNumber;
        run->setExpiryDate(RunInfo::Expired);
        run->save();
        m_fileDeleter->add(*run);
    }

    if (next > 0)
    {
        std::lock_guard<std::mutex> lock(m_lock);
        if ((m_next < 0) || (next < m_next))
            m_next = next;
    }
}

bool RFMManager::preStart()
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (m_started)
    {
        LOG(ERROR) << "RFMManager already started";
        return false;
    }
    else if (!m_mon)
    {
        LOG(ERROR) << "RFMManager not initialized";
        return false;
    }
    m_started = true;
    return true;
}

bool RFMManager::run()
{
    if (!preStart())
        return false;

    thread_func();
    return true;
}

bool RFMManager::start()
{
    if (!preStart())
        return false;

    m_thread.reset(new std::thread(&RFMManager::thread_func, this));
    return true;
}

void RFMManager::stop()
{
    std::unique_lock<std::mutex> lock(m_lock);
    m_started = false;
    m_cond.notify_all();
    lock.unlock();

    if (m_thread)
    {
        m_thread->join();
        m_thread.reset();
    }
}

void RFMManager::completeFile(MergedFile &file)
{
    RunInfo::Shared run = RFMFactory::instance().getRun(file.runNumber());

    if (file.status() == MergedFile::IGNORED)
    {
        LOG(WARNING) << "[RFMManager] refusing to merge IGNORED file: "
                     << file.runNumber() << ":" << file.fileNumber();
        return;
    }
    else if (file.status() == MergedFile::WAITING)
    {
        /* unlikely to happen */
        LOG(WARNING)
            << "[RFMManager] file already queued : " << file.runNumber() << ":"
            << file.fileNumber();
        return;
    }

    file.setStatus((run && run->isApproved()) ?
                       MergedFile::WAITING :
                       MergedFile::WAITING_FOR_APPROVAL);
    file.save();

    if (file.status() == MergedFile::WAITING)
    {
        MergerTask *task = new MergerTask(file);
        task->taskSignal.connect([this, task](const std::string &msg) {
            MergedFile::Shared file = task->mergedFile();
            if (!file)
            {
                LOG(ERROR)
                    << "[RFMManager] corrupted merger: no associated file";
            }
            else if (!msg.empty())
            {
                LOG(ERROR)
                    << "[RFMManager] merger task error occurred: " << msg;
                this->errorOccured(*file, msg);
            }
            else
            {
                LOG(INFO) << "[RFMManager] merger task completed: "
                          << file->fileName();
                this->m_mon->setError(file->fileName());   // clear
                this->m_mon->setWarning(file->fileName()); // clear
                this->m_fileDeleter->add(*file);

                std::lock_guard<std::mutex> lock(m_lock);
                if (m_updates.insert(file->runNumber()).second)
                    m_cond.notify_all();
            }
        });
        m_worker.post(Worker::SharedTask(task));
    }
}

void RFMManager::mergeIndex(MergedFileIndex &index)
{
    if ((index.status() == MergedFile::IGNORED) ||
        !index.setStatus(MergedFile::WAITING))
    {
        LOG(ERROR) << "[RFMManager] can't merge index, invalid status: "
                   << index.status();
        return;
    }
    else
        index.save();

    IndexerTask *task = new IndexerTask(index);
    task->taskSignal.connect([this, task](const std::string &msg) {
        MergedFileIndex::Shared file = task->indexFile();
        if (!file)
        {
            LOG(ERROR) << "[RFMManager] corrupted indexer: no associated file";
        }
        else if (!msg.empty())
        {
            LOG(ERROR) << "[RFMManager] indexer task error occurred: " << msg;
            this->errorOccured(*file, msg);
        }
        else
        {
            LOG(INFO) << "[RFMManager] indexer task completed for run#"
                      << task->runInfo()->runNumber();
            this->m_mon->setError(file->fileName());   // clear
            this->m_mon->setWarning(file->fileName()); // clear
            // Run completed, tell it to deleter
            this->m_fileDeleter->add(*file);
        }
    });
    m_worker.post(Worker::SharedTask(task));
}

void RFMManager::errorOccured(MergedFile &file, const std::string &errorMsg)
{
    file.setTransferred(0);
    file.setStatus(MergedFile::FAILED);

    std::ostringstream oss;
    oss << errorMsg << " (retry #" << file.retries() << ")";

    LOG(WARNING) << "[RFMManager] error on file " << file.fileName() << ": "
                 << oss.str();
    file.save();

    m_mon->setWarning(file.fileName(), oss.str());
}

bool RFMManager::addEvent(int32_t runNumber,
                          int32_t seqEventNumber,
                          int32_t timingEventNumber,
                          uint64_t fileSize)
{
    // If not existing => Create a new one
    // Check file size => More than maxTotalSize_ => Submit merge into queue
    LOG(INFO) << "[RFMManager] event " << timingEventNumber
              << " validated for run " << runNumber;

    RunInfo::Shared run = RFMFactory::instance().getRun(runNumber);
    if (!run)
    {
        LOG(ERROR) << "[RFMManager] unknown run " << runNumber;
        return false;
    }
    return addEvent(*run, seqEventNumber, timingEventNumber, fileSize);
}

bool RFMManager::addEvent(RunInfo &run,
                          int32_t seqEventNumber,
                          int32_t timingEventNumber,
                          uint64_t fileSize)
{
    if (run.hasStopDate())
    {
        LOG(ERROR) << "[RFMManager] run " << run.runNumber()
                   << " is stopped, ignoring event " << timingEventNumber;
        return false;
    }

    MergedFile::Shared file = run.currentFile();
    if (!file || file->status() != MergedFile::INCOMPLETE)
    {
        file = run.addFile();
    }
    if (!file)
    {
        LOG(ERROR) << "[RFMManager] failed to create file";
        return false;
    }

    if (file->addEvent(seqEventNumber, timingEventNumber, fileSize))
    {
        if (needsToMerge(*file))
            completeFile(*file);
        return true;
    }
    return false;
}

bool RFMManager::addDaqEvent(int32_t runNumber,
                             int32_t timingEventNumber,
                             uint64_t fileSize)
{
    LOG(INFO) << "[RFMManager] daq event " << timingEventNumber << " for run "
              << runNumber;

    RunInfo::Shared run = RFMFactory::instance().getRun(runNumber);
    if (!run)
    {
        LOG(ERROR) << "[RFMManager] unknown run " << runNumber;
        return false;
    }
    else if (!run->isStandalone())
    {
        LOG(INFO) << "[RFMManager] run " << runNumber
                  << " is not standalone, ignoring event " << timingEventNumber;
        return false;
    }
    return addEvent(*run, timingEventNumber, timingEventNumber, fileSize);
}

bool RFMManager::needsToMerge(const MergedFile &file) const
{
    const Config &conf = Config::instance();
    LOG(DEBUG)
        << "Checking if file " << file.fileName() << " needs to be merged";

    // Check if file is candidate for merging
    if (file.size() >= conf.getMinTotalSize())
    {
        LOG(INFO) << "Minimum size reached on merged file " << file.fileName()
                  << " will merge";
        return true;
    }
    else if (file.eventsCount() >= conf.getMaxEvents())
    {
        LOG(INFO) << "Maximum events number reached on merged file "
                  << file.fileName() << " will merge";
        return true;
    }
    return false;
}
