/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-18T22:42:11+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include <DaqTypes.h>
#include <NTOFLogging.hpp>

#include "Config.h"
#include "Paths.hpp"
#include "RFMCli.hpp"
#include "RFMException.hpp"
#include "data/DaqInfo.hpp"
#include "data/RunInfo.hpp"

#include <dic.hxx>

namespace po = boost::program_options;
namespace bfs = boost::filesystem;
using namespace ntof::rfm;
using namespace ntof::log;
using namespace ntof::dim;

typedef std::vector<std::string> VArgs;

int import(const std::string &name, VArgs &vargs);

int startRun(const std::string &name, VArgs &vargs)
{
    RunInfo::RunNumber runNumber;
    DaqInfo::CrateIdList daqs;
    std::string experiment;
    bool approve;
    bool standalone;
    LOG(ERROR) << "HERE" << vargs;

    po::options_description desc("StartRun Options");
    // clang-format off
    desc.add_options()
    ("help", "produce this help message")
    ("runNumber", po::value(&runNumber)->required(),
        "Run number to start")
    ("experiment", po::value(&experiment), "Run experiment")
    ("approve", "Approve the run")
    ("standalone", "Flag run as standalone (run/event files will be retrieved on daq)")
    ("daqs", po::value<std::string>()->required(),
        "List of daq (':' separated)");
    // clang-format on

    po::variables_map vm;
    po::store(po::command_line_parser(vargs).options(desc).run(), vm);

    if (vm.count("help"))
    {
        std::cout << "Usage: " << name << " startRun [options]\n\n"
                  << desc << std::endl;
        po::notify(vm);
        return 0;
    }
    po::notify(vm);

    approve = vm.count("approve") != 0;
    standalone = vm.count("standalone") != 0;
    DaqInfo::fromString(vm["daqs"].as<std::string>(), daqs);

    RFMCli cli;
    if (!cli.cmdStart(runNumber, daqs, experiment, &approve, &standalone))
        throw RFMException("Failed to create run");
    std::cout << "Run created" << std::endl;
    return 0;
}

int editRun(const std::string &name, VArgs &vargs)
{
    RunInfo::RunNumber runNumber;
    std::string experiment;
    std::time_t expiry;

    po::options_description desc("EditRun Options");
    // clang-format off
    desc.add_options()
    ("help", "produce this help message")
    ("runNumber", po::value(&runNumber)->required(),
        "Run number to edit")
    ("experiment", po::value(&experiment), "set experiment")
    ("approve", "Approve the run")
    ("expiry", po::value(&expiry), "modify expiry (-1 to disable)");
    // clang-format on

    po::variables_map vm;
    po::store(po::command_line_parser(vargs).options(desc).run(), vm);

    if (vm.count("help"))
    {
        std::cout << "Usage: " << name << " editRun [options]\n\n"
                  << desc << std::endl;
        po::notify(vm);
        return 0;
    }
    po::notify(vm);

    RFMCli cli;
    if (vm.count("experiment") != 0)
    {
        if (!cli.setRunExperiment(runNumber, experiment))
            throw RFMException("Failed to set experiment");
    }

    if (vm.count("expiry") != 0)
    {
        if (!cli.setRunExpiry(runNumber, expiry))
            throw RFMException("Failed to set expiry");
    }
    if (vm.count("approve") != 0)
    {
        if (!cli.setRunApproved(runNumber))
            throw RFMException("Failed to approve run");
    }
    std::cout << "Run modified" << std::endl;
    return 0;
}

int getFiles(const std::string &name, VArgs &vargs)
{
    RunInfo::RunNumber runNumber;

    po::options_description desc("StartRun Options");
    // clang-format off
    desc.add_options()
    ("help", "produce this help message")
    ("runNumber", po::value(&runNumber)->required(),
        "Run number");
    // clang-format on

    po::variables_map vm;
    po::store(po::command_line_parser(vargs).options(desc).run(), vm);

    if (vm.count("help"))
    {
        std::cout << "Usage: " << name << " getFiles --runNumber=1234\n\n"
                  << desc << std::endl;
        po::notify(vm);
        return 0;
    }
    po::notify(vm);

    RFMCli cli;
    std::vector<DIMData> data = cli.getFiles(runNumber);
    CLOG(INFO, "out") << ntof::log::debug() << "Files: " << data;
    return 0;
}

int listen(const std::string &name, VArgs &vargs)
{
    po::options_description desc("ListenMode Options");
    // clang-format off
    desc.add_options()
    ("help", "produce this help message")
    ("runs", "Listen for run events")
    ("current", "Listen for current run events")
    ("files", "Listen for file events");
    // clang-format on

    po::variables_map vm;
    po::store(po::command_line_parser(vargs).options(desc).run(), vm);

    if (vm.count("help"))
    {
        std::cout << "Usage: " << name << " listen [options]\n\n"
                  << desc << std::endl;
        po::notify(vm);
        return 0;
    }
    po::notify(vm);

    bool all = !vm.count("runs") && !vm.count("current") && !vm.count("files");
    RFMCli cli;
    cli.listen();
    if (all || vm.count("runs"))
    {
        cli.runsSignal.connect([](DIMParamListClient &cli) {
            CLOG(INFO, "out")
                << ntof::log::debug() << "/Runs : " << cli.getParameters();
        });
    }
    if (all || vm.count("current"))
    {
        cli.currentSignal.connect([](DIMParamListClient &cli) {
            CLOG(INFO, "out")
                << ntof::log::debug() << "/Current : " << cli.getParameters();
        });
    }
    if (all || vm.count("files"))
    {
        cli.filesSignal.connect([](DIMDataSetClient &cli) {
            CLOG(INFO, "out") << ntof::log::debug()
                              << "/Current/Files : " << cli.getLatestData();
        });
    }

    std::cout << "Listening..." << std::endl;
    while (true)
        std::this_thread::sleep_for(std::chrono::seconds(1));
    return 0;
}

int stopRun(const std::string &name, VArgs &vargs)
{
    RunInfo::RunNumber runNumber;
    DaqInfo::CrateIdList daqs;

    po::options_description desc("StopRun Options");
    // clang-format off
    desc.add_options()
    ("help", "produce this help message")
    ("runNumber", po::value(&runNumber)->required(),
        "Run number to stop");
    // clang-format on

    po::variables_map vm;
    po::store(po::command_line_parser(vargs).options(desc).run(), vm);

    if (vm.count("help"))
    {
        std::cout << "Usage: " << name << " stopRun [options]\n\n"
                  << desc << std::endl;
        po::notify(vm);
        return 0;
    }
    po::notify(vm);

    RFMCli cli;
    if (!cli.cmdStop(runNumber))
        throw RFMException("Failed to stop run");
    std::cout << "Run stopped" << std::endl;
    return 0;
}

int ignoreFile(const std::string &name, VArgs &vargs)
{
    RunInfo::RunNumber runNumber;
    MergedFile::FileNumber fileNumber;
    bool ignore = true;

    po::options_description desc("IgnoreFile Options");
    // clang-format off
    desc.add_options()
    ("help", "produce this help message")
    ("runNumber", po::value(&runNumber)->required(),
        "Run number")
    ("fileNumber", po::value(&fileNumber)->required(),
        "File number")
    ("ignore", po::value(&ignore)->default_value(true), "(un)ignore flag");
    // clang-format on

    po::variables_map vm;
    po::store(po::command_line_parser(vargs).options(desc).run(), vm);

    if (vm.count("help"))
    {
        std::cout << "Usage: " << name << " ignoreFile [options]\n\n"
                  << desc << std::endl;
        po::notify(vm);
        return 0;
    }
    po::notify(vm);

    RFMCli cli;
    if (!cli.cmdFileIgnore(runNumber, fileNumber, ignore))
        throw RFMException("Failed to ignore file");
    std::cout << "File " << (ignore ? "ignored" : "unignored") << std::endl;
    return 0;
}

int event(const std::string &name, VArgs &vargs)
{
    RunInfo::RunNumber runNumber;
    FileEvent::SequenceNumber seq;
    FileEvent::EventNumber event;
    std::size_t size;

    po::options_description desc("Event Options");
    // clang-format off
    desc.add_options()
    ("help", "produce this help message")
    ("runNumber", po::value(&runNumber)->required(),
        "Run number")
    ("seqNumber", po::value(&seq)->required(),
        "Event sequence number")
    ("eventNumber", po::value(&event)->required(),
        "Event number")
    ("size", po::value(&size)->default_value(0), "data size");
    // clang-format on

    po::variables_map vm;
    po::store(po::command_line_parser(vargs).options(desc).run(), vm);

    if (vm.count("help"))
    {
        std::cout << "Usage: " << name << " ignoreFile [options]\n\n"
                  << desc << std::endl;
        po::notify(vm);
        return 0;
    }
    po::notify(vm);

    RFMCli cli;
    if (!cli.cmdEvent(runNumber, seq, event, size))
        throw RFMException("Failed add event");
    std::cout << "Event added" << std::endl;
    return 0;
}

int main(int argc, char **argv)
{
    START_EASYLOGGINGPP(argc, argv);
    std::string config;
    std::string configMisc;
    std::string command;

    try
    {
        po::options_description desc("Global Options");
        // clang-format off
        desc.add_options()
        ("help", "produce this help message")
        ("config,c",
            po::value(&config)->default_value(Config::configFile),
            "force the configuration file")
        ("misc-config,m",
            po::value(&configMisc)
                ->default_value(Config::configMiscFile),
            "force the misc configuration file")(
            "command", po::value(&command),
            "Command to execute:\n"
            "  startRun: start a new run\n"
            "  stopRun: stop an ongoing run\n"
            "  event: record an event on a run\n"
            "  editRun: edit run parameters\n"
            "  ignoreFile: ignore a failed file\n"
            "  listen: listen for events\n"
            "  getFiles: list files in a run\n"
            "  import: import an existing run"
        )
        ("verbose", "Turn on maximum verbosity");

        po::options_description extra = desc;
        extra.add_options()
        ("args...", po::value<std::vector<std::string>>(),
            "command specific arguments");
        // clang-format on

        po::positional_options_description pd;
        pd.add("command", 1);
        // ignore unknown positionals, and let it go to the next level
        pd.add("args...", -1);

        po::variables_map vm;
        po::parsed_options parsed = po::command_line_parser(argc, argv)
                                        .options(extra)
                                        .positional(pd)
                                        .allow_unregistered()
                                        .run();
        std::vector<std::string> unparsed = po::collect_unrecognized(
            parsed.options, po::include_positional);
        po::store(parsed, vm);
        po::notify(vm);

        if (vm.count("help"))
        {
            unparsed.push_back("--help");
        }

        if (!command.empty())
        {
            {
                Config::load(config, configMisc);
                Config &conf = Config::instance();
                DimClient::setDnsNode(conf.getDimDns().c_str(),
                                      conf.getDimDnsPort());
            }

            {
                el::Configurations conf;
                conf.setToDefault();
                conf.set(el::Level::Global, el::ConfigurationType::Format,
                         "%level: %msg");
                conf.set(el::Level::Global, el::ConfigurationType::ToFile,
                         "false");
                conf.set(el::Level::Global,
                         el::ConfigurationType::ToStandardOutput, "true");
                conf.set(el::Level::Global, el::ConfigurationType::Enabled,
                         vm.count("verbose") ? "true" : "false");
                el::Loggers::reconfigureAllLoggers(conf);

                conf.set(el::Level::Global, el::ConfigurationType::Enabled,
                         "true");
                conf.set(el::Level::Global, el::ConfigurationType::Format,
                         "%msg");
                el::Logger *l = el::Loggers::getLogger("out", true);
                l->configure(conf);
            }

            if (command == "startRun")
                return startRun(argv[0], unparsed);
            else if (command == "stopRun")
                return stopRun(argv[0], unparsed);
            else if (command == "listen")
                return listen(argv[0], unparsed);
            else if (command == "getFiles")
                return getFiles(argv[0], unparsed);
            else if (command == "editRun")
                return editRun(argv[0], unparsed);
            else if (command == "ignoreFile")
                return ignoreFile(argv[0], unparsed);
            else if (command == "event")
                return event(argv[0], unparsed);
            else if (command == "import")
                return import(argv[0], unparsed);
        }

        std::cout
            << "Usage:\n"
            << argv[0]
            << " [startRun|stopRun|editRun|event|ignoreFile...] <opts>\n\n"
            << desc << std::endl;
        return 0;
    }
    catch (const std::exception &ex)
    {
        std::cerr << "Error: " << ex.what() << std::endl;
        return -1;
    }
    return 0;
}
