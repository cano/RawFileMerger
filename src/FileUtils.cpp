/*
 * FileUtils.cpp
 *
 *  Created on: Nov 6, 2015
 *      Author: mdonze
 */

#include "FileUtils.hpp"

#include <sstream>

#include <boost/filesystem.hpp>

#include <easylogging++.h>
#include <sys/stat.h>

#include "NTOFException.h"
#include "RFMException.hpp"

using namespace boost::filesystem;

using namespace ntof::rfm;

/**
 * Get the size of a file.
 * @param filename The name of the file to check size for
 * @return The filesize, or 0 if the file does not exist.
 */
size_t FileUtils::getFilesize(const std::string &filename)
{
    try
    {
        LOG(TRACE) << "Getting size of file " << filename;
        return file_size(filename);
    }
    catch (const std::exception &ex)
    {
        std::ostringstream oss;
        oss << "Unable to get size of file " << filename << " (" << ex.what()
            << ")";
        LOG(ERROR) << oss.str();
        throw RFMException(oss.str());
    }
    catch (...)
    {
        std::ostringstream oss;
        oss << "Unexpected exception while getting size of file " << filename;
        LOG(ERROR) << oss.str();
        throw RFMException(oss.str());
    }
}

/**
 * Delete a file in the file system
 * @param fileName Full path of the file to be deleted
 */
void FileUtils::deleteFile(const std::string &fileName)
{
    try
    {
        LOG(TRACE) << "Deleting file " << fileName;
        remove(fileName);
    }
    catch (const std::exception &ex)
    {
        std::ostringstream oss;
        oss << "Unable to delete file " << fileName << " (" << ex.what() << ")";
        LOG(ERROR) << oss.str();
        throw RFMException(oss.str());
    }
    catch (...)
    {
        std::ostringstream oss;
        oss << "Unexpected exception while deleting file " << fileName;
        LOG(ERROR) << oss.str();
        throw RFMException(oss.str());
    }
}

/**
 * Delete a directory in the file system (and all children)
 * @param fileName Full path of the folder to be deleted
 */
void FileUtils::deleteDirectory(const std::string &fileName)
{
    try
    {
        LOG(TRACE) << "Deleting folder " << fileName;
        remove_all(fileName);
    }
    catch (const std::exception &ex)
    {
        std::ostringstream oss;
        oss << "Unable to delete directory " << fileName << " (" << ex.what()
            << ")";
        LOG(ERROR) << oss.str();
        throw RFMException(oss.str());
    }
    catch (...)
    {
        std::ostringstream oss;
        oss << "Unexpected exception while deleting directory " << fileName;
        LOG(ERROR) << oss.str();
        throw RFMException(oss.str());
    }
}
