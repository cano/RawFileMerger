/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-03T09:34:03+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef FILE_EVENT_HPP__
#define FILE_EVENT_HPP__

#include <cstdint>
#include <memory>
#include <mutex>

#include "Storable.hpp"
#include "Types.hpp"

namespace ntof {
namespace rfm {

class RFMDatabase;
class RFMFactory;
class MergedFile;

/**
 * @brief Event information object
 * @details use RFMFactory to retrieve/manipulate this object
 * @details all public methods are thread safe
 */
class FileEvent : public Storable<FileEvent>
{
public:
    typedef std::shared_ptr<FileEvent> Shared;
    typedef uint32_t EventNumber;
    typedef uint32_t SequenceNumber;

    FileEvent(const FileEvent &) = delete;
    FileEvent &operator=(const FileEvent &) = delete;

    /**
     * @brief get associated run number
     */
    inline RunNumber runNumber() const { return m_runNumber; }

    /**
     * @brief get file sequence number
     */
    inline FileNumber fileNumber() const { return m_fileNumber; }

    /**
     * @brief get event number
     */
    inline FileEvent::EventNumber eventNumber() const { return m_eventNumber; }

    /**
     * @brief get event sequence number
     */
    inline FileEvent::SequenceNumber sequenceNumber() const { return m_seqNum; }

    /**
     * @brief retrieve event offset in file
     */
    std::size_t offset() const;

    /**
     * @brief check if this FileEvent has an offset set
     * @details size_t is unsigned, thus this function
     */
    inline bool hasOffset() const { return offset() != std::size_t(-1); }

    /**
     * @brief set event offset
     */
    void setOffset(std::size_t offset);

    std::size_t size() const;

    void setSize(std::size_t size);

protected:
    friend class RFMDatabase;
    friend class RFMFactory;

    FileEvent(const MergedFile &file,
              SequenceNumber seqNum,
              EventNumber eventNumber,
              std::size_t size,
              std::size_t offset = std::size_t(-1));

    const RunNumber m_runNumber;
    const FileNumber m_fileNumber;
    const SequenceNumber m_seqNum;
    const EventNumber m_eventNumber;
    std::size_t m_size;
    std::size_t m_offset;
};

} // namespace rfm
} // namespace ntof

#endif
