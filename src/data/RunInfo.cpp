/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-27T13:21:18+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "RunInfo.hpp"

#include <limits>

#include <easylogging++.h>
#include <time.h>

#include "RFMFactory.hpp"

#include "Storable.hxx"

using namespace ntof::rfm;
using namespace ntof::utils;

template class ntof::rfm::Storable<RunInfo>;

const RunInfo::RunNumber RunInfo::InvalidRunNumber =
    std::numeric_limits<RunInfo::RunNumber>::max();
const std::time_t RunInfo::NoExpiry = std::time_t(-1);
const std::time_t RunInfo::Expired = std::time_t(-2);

RunInfo::RunInfo(RunInfo::RunNumber runNumber,
                 const std::string &expArea,
                 std::time_t startDate) :
    m_runNumber(runNumber),
    m_expArea(expArea),
    m_startDate(startDate),
    m_stopDate(-1),
    m_expiryDate(-1),
    m_flags(0),
    m_transferred(0)
{}

RunInfo::~RunInfo()
{
    RFMFactory::release(*this);
}

int RunInfo::year() const
{
    std::time_t start = startDate();
    struct tm ret;

    if (::localtime_r(&start, &ret) == nullptr)
    {
        LOG(ERROR) << "failed to retrieve localtime";
        return 1970;
    }
    return ret.tm_year + 1900;
}

std::time_t RunInfo::stopDate() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_stopDate;
}

bool RunInfo::setStopDate(std::time_t stop)
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (m_stopDate != std::time_t(-1))
        return false;
    m_stopDate = stop;
    return true;
}

std::time_t RunInfo::expiryDate() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_expiryDate;
}

bool RunInfo::setExpiryDate(std::time_t date)
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (!(m_flags & Approved))
    {
        m_expiryDate = date;
        return true;
    }
    return false;
}

std::set<DaqInfo::CrateId> RunInfo::getDaqs() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_daqs;
}

bool RunInfo::hasDaq(DaqInfo::CrateId id) const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_daqs.count(id) != 0;
}

std::string RunInfo::experiment() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_experiment;
}

bool RunInfo::setExperiment(const std::string &name)
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (!(m_flags & Approved))
    {
        m_experiment = name;
        return true;
    }
    return false;
}

FileStats RunInfo::fileStats() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_stats;
}

std::size_t RunInfo::transferredBytes() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_transferred;
}

void RunInfo::setTransferredBytes(std::size_t size)
{
    std::lock_guard<std::mutex> lock(m_lock);
    m_transferred = size;
}

bool RunInfo::isStandalone() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return (m_flags & Standalone) ? true : false;
}

void RunInfo::setStandalone(bool value)
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (value)
        m_flags |= Standalone;
    else
        m_flags &= ~Standalone;
}

bool RunInfo::isApproved() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return (m_flags & Approved) ? true : false;
}

bool RunInfo::setApproved(bool approve)
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (!approve)
        return false; /* can't un-approve a run */

    /* approving clears expiry */
    m_expiryDate = RunInfo::NoExpiry;
    m_flags |= Approved;
    return true;
}

void RunInfo::addStats(const MergedFile &file)
{
    MergedFile::Status status = file.status();
    ++m_stats[status];
    switch (status)
    {
    case MergedFile::TRANSFERRING: m_transferred += file.transferred(); break;
    case MergedFile::COPIED:
    case MergedFile::MIGRATED: m_transferred += file.size(); break;
    default: break;
    }
}

void RunInfo::updateStats()
{
    std::lock_guard<std::mutex> lock(m_lock);
    m_transferred = 0;
    m_stats.clear();
    for (MergedFile::Shared &file : m_files)
    {
        addStats(*file);
    }

    if (m_index)
    {
        addStats(*m_index);
    }
}

MergedFile::FileNumber RunInfo::getNextFileNumber() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    MergedFile::FileNumber num = 0;

    for (const MergedFile::Shared &shared : m_files)
    {
        if (shared->fileNumber() >= num)
        {
            num = shared->fileNumber() + 1;
        }
    }
    return num;
}

MergedFile::Shared RunInfo::addFile()
{
    if (hasStopDate())
    {
        LOG(WARNING) << "can't add file on stopped run:" << runNumber();
        return MergedFile::Shared();
    }
    return RFMFactory::instance().addFile(*this);
}

MergedFile::Shared RunInfo::currentFile() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (!m_files.empty())
        return m_files.back();
    return MergedFile::Shared();
}

RunInfo::FilesList RunInfo::getFiles() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_files;
}

MergedFile::Shared RunInfo::findFile(MergedFile::FileNumber number) const
{
    std::lock_guard<std::mutex> lock(m_lock);
    for (const MergedFile::Shared &shared : m_files)
    {
        if (shared->fileNumber() == number)
            return shared;
    }
    return MergedFile::Shared();
}

MergedFileIndex::Shared RunInfo::createIndex()
{
    if (!hasStopDate())
    {
        LOG(WARNING) << "can't add index on ongoing run:" << runNumber();
        return MergedFileIndex::Shared();
    }
    else if (m_index)
    {
        return m_index;
    }
    return RFMFactory::instance().createIndex(*this);
}
