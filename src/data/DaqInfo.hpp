/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-27T13:33:50+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef DAQINFO_HPP__
#define DAQINFO_HPP__

#include <memory>
#include <set>

#include <boost/filesystem.hpp>

namespace ntof {
namespace rfm {

class RFMFactory;
class RunInfo;
class FileEvent;

/**
 * @brief Daq information object
 * @details use RFMFactory to retrieve/manipulate this object
 * @details all public methods are thread safe
 */
class DaqInfo
{
public:
    typedef std::shared_ptr<DaqInfo> Shared;
    typedef int32_t CrateId;
    typedef std::set<CrateId> CrateIdList;

    DaqInfo(const DaqInfo &) = delete;
    DaqInfo &operator=(const DaqInfo &) = delete;

    /**
     * @brief retrieve the Crate Id
     */
    inline CrateId crateId() const { return m_crateId; }

    /**
     * @brief retrieve the Daq name
     * @details likely to be its hostname without domain (ntofdaq-m1)
     */
    inline const std::string &name() const { return m_name; }

    /**
     * @brief retrieve the Daq hostname
     * @details likely to have included domain (ntofdaq-m1.cern.ch)
     */
    inline const std::string &hostname() const { return m_hostname; }

    /**
     * @brief serialize a CrateId list to string
     * @param[in]  daqs the CrateId list to serialize
     * @param[out]  out the output colon separated (":") CrateId string list
     * @return true on success, false on error
     */
    static bool toString(const CrateIdList &daqs, std::string &out);

    /**
     * @brief de-serialize a CrateId list from a string
     * @param[in]  daqs colon (":") separated CrateId list
     * @param[out]  out the output CrateId list
     * @return true on success, false on error
     */
    static bool fromString(const std::string &daqs, CrateIdList &out);

protected:
    friend class RFMFactory;

    DaqInfo(CrateId crateId, const std::string &hostname);

    const CrateId m_crateId;
    const std::string m_hostname;
    const std::string m_name;
};

} // namespace rfm
} // namespace ntof

#endif
