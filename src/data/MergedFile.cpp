/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-27T14:26:10+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "MergedFile.hpp"

#include <ctime>
#include <limits>
#include <sstream>

#include <easylogging++.h>

#include "Storable.hxx"

using namespace ntof::rfm;

template class ntof::rfm::Storable<MergedFile>;

const MergedFile::FileNumber MergedFile::InvalidFileNumber =
    std::numeric_limits<MergedFile::FileNumber>::max();

static const std::string s_empty;

const MergedFile::FileNumber MergedFile::IndexFileNumber = -1;
const MergedFile::StatusNameMap MergedFile::statusNames{
    {MergedFile::INCOMPLETE, "incomplete"},
    {MergedFile::WAITING_FOR_APPROVAL, "waitingApproval"},
    {MergedFile::WAITING, "waiting"},
    {MergedFile::TRANSFERRING, "transferring"},
    {MergedFile::COPIED, "copied"},
    {MergedFile::MIGRATED, "migrated"},
    {MergedFile::IGNORED, "ignored"},
    {MergedFile::FAILED, "failed"}};

MergedFile::MergedFile(RunNumber runNumber, MergedFile::FileNumber segNum) :
    m_runNumber(runNumber),
    m_fileNumber(segNum),
    m_status(MergedFile::INCOMPLETE),
    m_size(0),
    m_transferred(0),
    m_eventsSize(0),
    m_retries(0),
    m_retryDate(std::time_t(-1))
{}

std::string MergedFile::fileName() const
{
    std::ostringstream oss;
    oss << "run" << m_runNumber << "_" << m_fileNumber << "_s1.raw.finished";
    return oss.str();
}

MergedFile::Status MergedFile::status() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_status;
}

bool MergedFile::check(MergedFile::Status from, MergedFile::Status to) const
{
    if (from == to)
        return true;
    switch (to)
    {
    case INCOMPLETE: return false;
    case WAITING_FOR_APPROVAL:
        return (from == INCOMPLETE) || (from == FAILED) || (from == IGNORED);
    case WAITING:
        return (from == INCOMPLETE) || (from == WAITING_FOR_APPROVAL) ||
            (from == FAILED) || (from == IGNORED);
    case TRANSFERRING: return from == WAITING;
    case COPIED: return from == TRANSFERRING;
    case MIGRATED: return from == COPIED;
    case IGNORED:
    case FAILED: return true;
    }
    LOG(ERROR) << "[MergedFile] unknown status: " << to;
    return false;
}

bool MergedFile::setStatus(MergedFile::Status status)
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (check(m_status, status))
    {
        if (status == FAILED)
        {
            if (m_status != status)
            {
                m_retryDate = std::time(nullptr) + (1 << m_retries);
                ++m_retries;
            }
        }
        else if (m_status == FAILED)
        {
            /* getting out of error: suspend retry timer */
            m_retryDate = -1;
        }
        m_status = status;
        return true;
    }
    LOG(ERROR) << "[MergedFile] invalid transition " << toString(m_status)
               << " -> " << toString(status) << " for " << fileName();
    return false;
}

void MergedFile::clearStatus()
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (m_status != IGNORED)
        m_status = FAILED;
}

std::size_t MergedFile::size() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_size;
}

void MergedFile::setSize(std::size_t size)
{
    std::lock_guard<std::mutex> lock(m_lock);
    m_size = size;
}

std::size_t MergedFile::transferred() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_transferred;
}

void MergedFile::setTransferred(std::size_t size)
{
    std::lock_guard<std::mutex> lock(m_lock);
    m_transferred = size;
}

MergedFile::EventsList MergedFile::getEvents() const
{
    RFMFactory::instance().loadEvents(const_cast<MergedFile &>(*this));

    std::lock_guard<std::mutex> lock(m_lock);
    return (m_events) ? *m_events : EventsList();
}

std::size_t MergedFile::eventsCount() const
{
    RFMFactory::instance().loadEvents(const_cast<MergedFile &>(*this));

    std::lock_guard<std::mutex> lock(m_lock);
    return (m_events) ? m_events->size() : 0;
}

std::size_t MergedFile::eventsSize() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_eventsSize;
}

FileEvent::Shared MergedFile::addEvent(FileEvent::SequenceNumber seqNum,
                                       FileEvent::EventNumber eventNumber,
                                       std::size_t fileSize)
{
    return RFMFactory::instance().addEvent(*this, seqNum, eventNumber, fileSize);
}

uint32_t MergedFile::retries() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_retries;
}

std::time_t MergedFile::retryDate() const
{
    std::lock_guard<std::mutex> lock(m_lock);
    return m_retryDate;
}

const std::string &MergedFile::toString(MergedFile::Status status)
{
    StatusNameMap::const_iterator it = MergedFile::statusNames.find(status);
    if (it == MergedFile::statusNames.end())
        return s_empty;
    else
        return it->second;
}
