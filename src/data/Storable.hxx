/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-03T15:45:20+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef STORABLE_HXX__
#define STORABLE_HXX__

#include "RFMFactory.hpp"
#include "Storable.hpp"

namespace ntof {
namespace rfm {

template<class S>
bool Storable<S>::save(bool wait)
{
    ntof::utils::Worker::SharedTask task;
    {
        std::lock_guard<std::mutex> lock(m_lock);
        if (!m_storeTask)
        {
            m_storeTask = RFMFactory::instance().save(*static_cast<S *>(this));
        }
        task = m_storeTask;
    }
    if (wait)
    {
        return task && task->wait() &&
            (task->error() == ntof::utils::Worker::Task::OK);
    }
    else
    {
        return bool(task);
    }
}

} // namespace rfm
} // namespace ntof

#endif
