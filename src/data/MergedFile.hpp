/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-27T13:50:30+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef MERGEDFILE_HPP__
#define MERGEDFILE_HPP__

#include <cstddef>
#include <cstdint>
#include <map>
#include <memory>
#include <string>

#include <Worker.hpp>

#include "FileEvent.hpp"
#include "Storable.hpp"
#include "Types.hpp"

namespace ntof {
namespace rfm {

class RFMDatabase;
class RFMFactory;

/**
 * @brief Merged information object
 * @details use RFMFactory to retrieve/manipulate this object
 * @details all public methods are thread safe
 */
class MergedFile : public Storable<MergedFile>
{
public:
    typedef std::shared_ptr<MergedFile> Shared;
    typedef ntof::rfm::FileNumber FileNumber;
    typedef std::vector<FileEvent::Shared> EventsList;

    static const FileNumber IndexFileNumber;
    static const FileNumber InvalidFileNumber;

    enum Status
    {
        INCOMPLETE = 0,
        WAITING_FOR_APPROVAL = 1,
        WAITING = 2, // Files in this state have a pending MergerTask or
                     // IndexerTask
        TRANSFERRING = 3,
        COPIED = 4,
        MIGRATED = 5,
        IGNORED = 6,
        FAILED = 7
    };
    typedef std::map<Status, std::string> StatusNameMap;

    MergedFile(const MergedFile &) = delete;
    MergedFile &operator=(const MergedFile &) = delete;
    virtual ~MergedFile() = default;

    /**
     * @brief get file name
     */
    virtual std::string fileName() const;

    /**
     * @brief get associated run number
     */
    inline RunNumber runNumber() const { return m_runNumber; }

    /**
     * @brief get file sequence number
     */
    inline MergedFile::FileNumber fileNumber() const { return m_fileNumber; }

    /**
     * @brief get file status
     */
    MergedFile::Status status() const;

    /**
     * @brief update the file status
     * @param[in]  status the new status to set
     */
    bool setStatus(MergedFile::Status status);

    /**
     * @brief set status to FAILED but does not increment retries
     */
    void clearStatus();

    /**
     * @brief return the file size
     */
    std::size_t size() const;

    /**
     * @brief update the file size
     */
    void setSize(std::size_t size);

    /**
     * @brief get transferred bytes count
     */
    std::size_t transferred() const;

    /**
     * @brief update transferred bytes count
     */
    void setTransferred(std::size_t size);

    /**
     * @brief retrieve associated events
     */
    EventsList getEvents() const;

    /**
     * @brief retrieve events count
     * @return current event count
     */
    std::size_t eventsCount() const;

    /**
     * @brief retrieve the estimated size of the file regarding its events
     */
    std::size_t eventsSize() const;

    /**
     * @bbrief add an event
     */
    FileEvent::Shared addEvent(FileEvent::SequenceNumber seqNum,
                               FileEvent::EventNumber eventNumber,
                               std::size_t size);

    /**
     * @brief get current failures count
     * @details this is automatically incremented when going to FAILED state
     */
    uint32_t retries() const;

    /**
     * @brief retrieve retry date for this file
     */
    std::time_t retryDate() const;

    static const std::string &toString(Status status);

    static const StatusNameMap statusNames;

protected:
    friend class RFMDatabase;
    friend class RFMFactory;

    MergedFile(RunNumber runNumber, FileNumber segNum);

    bool check(Status from, Status to) const;

    const RunNumber m_runNumber;
    const FileNumber m_fileNumber;
    Status m_status;
    std::size_t m_size; /*<! size once generated */
    std::size_t m_transferred;
    std::size_t m_eventsSize; /*<! estimated events size */
    uint32_t m_retries;
    std::time_t m_retryDate;
    mutable std::unique_ptr<EventsList> m_events;
    mutable ntof::utils::Worker::SharedTask m_loadTask;
};

} // namespace rfm
} // namespace ntof

#endif
