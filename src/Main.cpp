/*
 * Main.cpp
 *
 *  Created on: Oct 8, 2015
 *      Author: mdonze
 */
#include <iostream>
#include <string>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include "NTOFException.h"
#include "RFMManager.hpp"

// Include logging stuff
#include <easylogging++.h>

#include "Config.h"

#include <dic.hxx>
#include <dis.hxx>

using namespace ntof::rfm;
namespace po = boost::program_options;
namespace bfs = boost::filesystem;

int main(int argc, char **argv)
{
    START_EASYLOGGINGPP(argc, argv);
    try
    {
        std::string configFile;
        std::string configMisc;
        std::string logConfFile;
        int vLevel;
        // Parse program option
        po::options_description desc("Program usage");
        desc.add_options()("help", "produce this help message")(
            "verbose", "verbose mode")("config,c",
                                       po::value<std::string>(&configFile)
                                           ->default_value(Config::configFile),
                                       "force the configuration file")(
            "misc-config,m",
            po::value<std::string>(&configMisc)
                ->default_value(Config::configMiscFile),
            "force the misc configuration file")("noremove,n",
                                                 "don't delete local files")(
            "logconf,l",
            po::value<std::string>(&logConfFile)
                ->default_value("/etc/ntof/rfmerger_logger.conf"),
            "force logger configuration file")(
            "v,v", po::value<int>(&vLevel)->default_value(0),
            "Sets verbose level");
        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);

        if (vm.count("help"))
        {
            std::cout << desc << "\n";
            return 0;
        }

        po::notify(vm);

        // Initialize logger
        el::Configurations confFromFile(logConfFile);
        el::Loggers::reconfigureAllLoggers(confFromFile);

        Config::load(configFile, configMisc);

        Config &conf = Config::instance();
        conf.setDeleterEnabled(vm.count("noremove") == 0);

        DimServer::setDnsNode(conf.getDimDns().c_str(), conf.getDimDnsPort());
        DimClient::setDnsNode(conf.getDimDns().c_str(), conf.getDimDnsPort());

        bfs::create_directories(
            bfs::path(conf.getPaths().database).parent_path());

        RFMManager man;
        man.initialize();

        DimServer::start(conf.getDimPrefix().c_str());

        man.run();
    }
    catch (const ntof::NTOFException &ex)
    {
        LOG(FATAL) << "Caught n_TOF exception : " << ex.what();
        LOG(INFO) << "Exiting...";
    }
    catch (const std::exception &ex)
    {
        LOG(FATAL) << "Caught exception : " << ex.what();
        LOG(INFO) << "Exiting...";
    }
    catch (...)
    {
        LOG(FATAL) << "Caught unknown exception...";
        LOG(INFO) << "Exiting...";
    }
    return 0;
}
