/*
 * RawHeaders.h
 *
 *  Created on: Jan 12, 2016
 *      Author: mdonze
 */

#ifndef RAWHEADERS_H_
#define RAWHEADERS_H_

#include <stdint.h>

typedef struct
{
    char title[4];      // Title
    uint32_t revNumber; // Revision number
    uint32_t reserved;  // Reserved flag
    uint32_t nbWords;   // Number of words following
} HEADER;

typedef struct
{
    HEADER header;            // Standard header
    uint32_t runNumber;       // Run number
    uint32_t fileNumber;      // File segment number
    uint32_t streamID;        // Stream identifier
    char experiment[4];       // Experimental area
    uint32_t startDate;       // Starting date
    uint32_t startTime;       // Starting time
    uint32_t nbModules;       // Number of modules in the stream
    uint32_t nbChannels;      // Number of channels in the stream
    uint32_t totalStreams;    // Total number of streams in the run
    uint32_t totalChassis;    // Total number of chassis in the run
    uint32_t totalModules;    // Total number of modules in the run
    uint32_t nbChanPerMod[0]; // Number of used channels per module
} RCTR;

typedef struct
{
    HEADER header;
    uint32_t evtWCount; // Size of event in words
    uint32_t evtNumber; // Event number
    uint32_t runNumber; // Run number
    uint32_t evtTime;   // Event time
    uint32_t evtDate;   // Event data
    uint32_t reserved_; // Padding data
    double compTS;      // Computer event time-stamp
    double bctTS;       // BCT acquisition event time-stamp
    float intensity;    // Beam intensity
    uint32_t beamType;  // Beam type
} EVEH;

typedef struct
{
    HEADER header;
    uint64_t start;
    uint64_t end;
} SKIP;

typedef struct
{
    uint32_t streamNumber; //!< Stream number
    uint32_t fileNumber;   //!< Segment number
    uint32_t eventNumber;  //!< Event (sequence) number
    uint64_t fileOffset;   //!< Event file offset
} INDX_DETAILS;

typedef struct
{
    HEADER header;
    INDX_DETAILS indexes[1];
} INDX;

#endif /* RAWHEADERS_H_ */
