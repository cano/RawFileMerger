/*
 * FileUtils.hpp
 *
 *  Created on: Nov 6, 2015
 *      Author: mdonze
 */

#ifndef FILEUTILS_HPP_
#define FILEUTILS_HPP_

#include <string>

namespace ntof {
namespace rfm {

class FileUtils
{
public:
    virtual ~FileUtils();

    /**
     * Gets the size of the specified file
     * @param filename
     * @return
     */
    static size_t getFilesize(const std::string &filename);

    /**
     * Delete a file in the file system
     * @param fileName Full path of the file to be deleted
     */
    static void deleteFile(const std::string &fileName);

    /**
     * Delete a directory in the file system (and all children)
     * @param fileName Full path of the folder to be deleted
     */
    static void deleteDirectory(const std::string &fileName);

private:
    FileUtils();
};

} // namespace rfm
} // namespace ntof

#endif /* FILEUTILS_HPP_ */
