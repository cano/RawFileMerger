/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-26T09:15:31+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "RFMDatabase.hpp"

#include <easylogging++.h>

#include "RFMException.hpp"
#include "SqliteHelper.hpp"
#include "data/DaqInfo.hpp"

using sqlite::StmtGuard;
using namespace ntof::rfm;

RFMDatabase::RFMDatabase(const std::string &dbName)
{
    int rc = sqlite3_open_v2(dbName.c_str(), &m_db,
                             SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE |
                                 SQLITE_OPEN_NOMUTEX | SQLITE_OPEN_PRIVATECACHE,
                             nullptr);
    if (!check(rc, "failed to open DB"))
    {
        sqlite3_close(m_db);
        m_db = nullptr;
    }
    createDb();
}

RFMDatabase::~RFMDatabase()
{
    if (m_db)
    {
        for (StmtMap::value_type &item : m_stmts)
        {
            sqlite3_finalize(item.second);
        }
        sqlite3_close(m_db);
    }
}

template<class C>
std::shared_ptr<C> RFMDatabase::makeShared(C *c)
{
    std::shared_ptr<C> shared(c);
    c->m_weakRef = shared;
    return shared;
}

void RFMDatabase::createDb()
{
    char *msg;
    int rc;

    if (!m_db)
        return;
    rc = sqlite3_exec(m_db, "PRAGMA foreign_keys = ON;", nullptr, nullptr, &msg);
    check(rc, "failed to enabled foreign keys", msg);

    rc = sqlite3_exec(m_db,
                      "CREATE TABLE IF NOT EXISTS runs("
                      "runNumber INTEGER PRIMARY KEY,"
                      "expArea TEXT NOT NULL,"
                      "startDate INTEGER NOT NULL,"
                      "stopDate INTEGER DEFAULT -1,"
                      "expiryDate INTEGER DEFAULT -1,"
                      "experiment TEXT,"
                      "daqList TEXT,"
                      "flags INTEGER DEFAULT 0"
                      ");",
                      nullptr, nullptr, &msg);
    check(rc, "failed to create runs db", msg);
    rc = sqlite3_exec(m_db,
                      "CREATE INDEX IF NOT EXISTS runsExpiryIdx "
                      "ON runs(expiryDate);",
                      nullptr, nullptr, &msg);
    check(rc, "failed to create runs expiry index", msg);

    rc = sqlite3_exec(m_db,
                      "CREATE TABLE IF NOT EXISTS files("
                      "runNumber INTEGER NOT NULL,"
                      "fileNumber INTEGER NOT NULL,"
                      "status INTEGER NOT NULL,"
                      "size INTEGER DEFAULT 0,"
                      "PRIMARY KEY (runNumber, fileNumber),"
                      "FOREIGN KEY(runNumber) REFERENCES "
                      "runs(runNumber) ON DELETE CASCADE);",
                      nullptr, nullptr, &msg);
    check(rc, "failed to create files db", msg);

    rc = sqlite3_exec(m_db,
                      "CREATE TABLE IF NOT EXISTS events("
                      "runNumber INTEGER NOT NULL,"
                      "fileNumber INTEGER NOT NULL,"
                      "seqNum INTEGER NOT NULL,"
                      "event INTEGER NOT NULL,"
                      "size INTEGER NOT NULL,"
                      "offset INTEGER NOT NULL,"
                      "UNIQUE (runNumber,fileNumber,seqNum),"
                      "FOREIGN KEY(runNumber,fileNumber) REFERENCES "
                      "files(runNumber,fileNumber) ON DELETE CASCADE);",
                      nullptr, nullptr, &msg);
    check(rc, "failed to create events db", msg);

    rc = sqlite3_exec(m_db,
                      "CREATE TABLE IF NOT EXISTS fileRetries("
                      "runNumber INTEGER NOT NULL,"
                      "fileNumber INTEGER NOT NULL,"
                      "retries INTEGER NOT NULL,"
                      "retryDate INTEGER NOT NULL,"
                      "PRIMARY KEY (runNumber, fileNumber),"
                      "FOREIGN KEY(runNumber, fileNumber) REFERENCES "
                      "files(runNumber, fileNumber) ON DELETE CASCADE);",
                      nullptr, nullptr, &msg);
    check(rc, "failed to create fileRetries db", msg);

    rc = sqlite3_exec(m_db, "DELETE FROM fileRetries;", nullptr, nullptr, &msg);
    check(rc, "failed to delete fileRetries content", msg);

    rc = sqlite3_exec(m_db,
                      "CREATE INDEX IF NOT EXISTS fileRetriesIdx "
                      "ON fileRetries(retryDate);",
                      nullptr, nullptr, &msg);
    check(rc, "failed to create fileRetries index", msg);
}

RunInfo::Shared RFMDatabase::insertRun(RunInfo::RunNumber runNumber,
                                       const std::string &expArea,
                                       std::time_t startDate,
                                       const std::set<DaqInfo::CrateId> &daqs,
                                       const std::string &experiment)
{
    int rc;
    StmtGuard stmt(m_stmts[RunInsert]);

    if (!stmt)
    {
        rc = sqlite3_prepare_v2(
            m_db,
            "INSERT INTO runs "
            "(runNumber,expArea,startDate,experiment,daqList) VALUES "
            "(?,?,?,?,?);",
            -1, &stmt.stmt, nullptr);
        if (check(rc, "failed to prepare runs INSERT statement"))
            m_stmts[RunInsert] = stmt.stmt;
        else
            return RunInfo::Shared();
    }

    stmt.bind(runNumber);
    stmt.bind(expArea);
    stmt.bind(startDate);
    stmt.bind(experiment);

    std::string daqsStr;
    if (!DaqInfo::toString(daqs, daqsStr))
    {
        /* unlikely to happen, good luck to code coverage that one */
        throw RFMException("failed to serialize daq list");
    }

    stmt.bind(daqsStr);
    rc = stmt.step();
    if (!check(rc, "failed to insert run"))
        return RunInfo::Shared();

    RunInfo *runInfo = new RunInfo(runNumber, expArea, startDate);
    runInfo->m_daqs = daqs;
    runInfo->m_experiment = experiment;
    return makeShared(runInfo);
}

RunInfo::Shared RFMDatabase::getRun(RunInfo::RunNumber runNumber)
{
    int rc;
    StmtGuard stmt(m_stmts[RunGet]);

    if (!stmt)
    {
        rc = sqlite3_prepare_v2(
            m_db,
            "SELECT "
            "expArea,startDate,stopDate,expiryDate,experiment,daqList,flags "
            "FROM runs WHERE runNumber = ? LIMIT 1",
            -1, &stmt.stmt, nullptr);
        if (check(rc, "failed to prepare runs SELECT statement"))
            m_stmts[RunGet] = stmt.stmt;
        else
            return RunInfo::Shared();
    }

    stmt.bind(runNumber);
    rc = stmt.step();
    if (rc == SQLITE_ROW)
    {
        const std::string expArea(stmt.get<std::string>());
        const std::time_t startDate(stmt.get<std::time_t>());

        RunInfo::Shared runInfo(
            makeShared(new RunInfo(runNumber, expArea, startDate)));
        runInfo->m_stopDate = stmt.get<std::time_t>();
        runInfo->m_expiryDate = stmt.get<std::time_t>();
        runInfo->m_experiment = stmt.get<std::string>();
        if (!DaqInfo::fromString(stmt.get<std::string>(), runInfo->m_daqs))
        {
            /* unlikely to happen, good luck to code coverage that one */
            throw RFMException("failed to deserialize daq list");
        }
        runInfo->m_flags = stmt.get<RunInfo::Flags>();
        if (!getMergedFiles(*runInfo, runInfo->m_files, runInfo->m_index))
        {
            return RunInfo::Shared();
        }
        runInfo->updateStats();
        return runInfo;
    }
    else
    {
        check(rc, "failed to select run");
        return RunInfo::Shared();
    }
}

bool RFMDatabase::removeRun(RunInfo::RunNumber runNumber)
{
    int rc;
    StmtGuard stmt(m_stmts[RunDelete]);

    if (!stmt)
    {
        rc = sqlite3_prepare_v2(m_db, "DELETE FROM runs WHERE runNumber = ?",
                                -1, &stmt.stmt, nullptr);
        if (check(rc, "failed to prepare filesRetries DELETE statement"))
            m_stmts[RunDelete] = stmt.stmt;
        else
            return false;
    }

    /* let the foreign keys do the job */
    stmt.bind(runNumber);
    rc = stmt.step();
    return check(rc, "failed to delete run");
}

bool RFMDatabase::getMigratedRuns(RFMDatabase::RunNumberList &out)
{
    int rc;
    StmtGuard stmt(m_stmts[RunGetMigrated]);

    if (!stmt)
    {
        rc = sqlite3_prepare_v2(m_db,
                                "SELECT runNumber FROM runs WHERE "
                                "stopDate > 0 AND flags & ? AND NOT EXISTS "
                                "(SELECT runNumber FROM files WHERE "
                                " files.runNumber = runs.runNumber AND "
                                " NOT (status = ? OR status = ?) LIMIT 1)",
                                -1, &stmt.stmt, nullptr);
        if (check(rc, "failed to prepare SELECT (migrated runs) statement"))
            m_stmts[RunGetMigrated] = stmt.stmt;
        else
            return false;
    }

    stmt.bind(RunInfo::Approved);
    stmt.bind(MergedFile::MIGRATED);
    stmt.bind(MergedFile::IGNORED);

    out.clear();
    for (rc = stmt.step(); rc == SQLITE_ROW; rc = stmt.step())
    {
        out.insert(static_cast<RunInfo::RunNumber>(stmt.get<int64_t>()));
    }

    return check(rc, "failed to select runs");
}

bool RFMDatabase::getAllRuns(RFMDatabase::RunNumberList &out)
{
    int rc;
    StmtGuard stmt;

    rc = sqlite3_prepare_v2(m_db,
                            "SELECT runNumber FROM runs ORDER BY runNumber ASC",
                            -1, &stmt.stmt, nullptr);
    if (!check(rc, "failed to prepare SELECT (all runs) statement"))
        return false;

    out.clear();
    for (rc = stmt.step(); rc == SQLITE_ROW; rc = stmt.step())
    {
        out.insert(static_cast<RunInfo::RunNumber>(stmt.get<int64_t>()));
    }

    return check(rc, "failed to select runs");
}

bool RFMDatabase::update(RunInfo &run)
{
    int rc;
    StmtGuard stmt(m_stmts[RunUpdate]);

    if (!stmt)
    {
        rc = sqlite3_prepare_v2(m_db,
                                "UPDATE runs SET stopDate = ?,expiryDate = ?,"
                                "experiment = ?,flags = ? "
                                "WHERE runNumber = ?",
                                -1, &stmt.stmt, nullptr);
        if (check(rc, "failed to prepare runs UPDATE statement"))
            m_stmts[RunUpdate] = stmt.stmt;
        else
            return false;
    }

    run.m_lock.lock();
    stmt.bind(run.m_stopDate);
    stmt.bind(run.m_expiryDate);
    stmt.bind(run.m_experiment);
    stmt.bind(run.m_flags);
    stmt.bind(run.m_runNumber);
    run.m_storeTask.reset(); // new updates will be queued
    run.m_lock.unlock();

    rc = stmt.step();
    return check(rc, "failed to update run");
}

MergedFile::Shared RFMDatabase::insertMergedFile(const RunInfo &run,
                                                 MergedFile::FileNumber sequence)
{
    int rc;
    StmtGuard stmt(m_stmts[FileInsert]);

    if (!stmt)
    {
        rc = sqlite3_prepare_v2(m_db,
                                "INSERT INTO files "
                                "(runNumber,fileNumber,status) VALUES "
                                "(?,?,?);",
                                -1, &stmt.stmt, nullptr);
        if (check(rc, "failed to prepare files INSERT statement"))
            m_stmts[FileInsert] = stmt.stmt;
        else
            return MergedFile::Shared();
    }

    stmt.bind(run.runNumber());
    stmt.bind(sequence);
    stmt.bind(MergedFile::INCOMPLETE);

    rc = stmt.step();
    if (!check(rc, "failed to insert file"))
        return MergedFile::Shared();

    return makeShared((sequence == MergedFile::IndexFileNumber) ?
                          (new MergedFileIndex(run.runNumber())) :
                          (new MergedFile(run.runNumber(), sequence)));
}

bool RFMDatabase::getMergedFiles(const RunInfo &run,
                                 std::vector<MergedFile::Shared> &out,
                                 MergedFileIndex::Shared &index)
{
    int rc;
    StmtGuard stmt(m_stmts[FileGet]);

    if (!stmt)
    {
        rc = sqlite3_prepare_v2(
            m_db,
            "SELECT files.fileNumber,status,size,retries,retryDate "
            "FROM files LEFT JOIN fileRetries "
            "ON (files.runNumber = fileRetries.runNumber "
            "AND files.fileNumber = fileRetries.fileNumber) "
            "WHERE files.runNumber = ? "
            "ORDER BY files.fileNumber ASC;",
            -1, &stmt.stmt, nullptr);
        if (check(rc, "failed to prepare files SELECT statement"))
            m_stmts[FileGet] = stmt.stmt;
        else
            return false;
    }

    out.clear();
    index.reset();
    stmt.bind(run.runNumber());
    for (rc = stmt.step(); rc == SQLITE_ROW; rc = stmt.step())
    {
        const MergedFile::FileNumber fileNumber =
            static_cast<MergedFile::FileNumber>(stmt.get<int64_t>());

        if (fileNumber == MergedFile::IndexFileNumber)
        {
            MergedFileIndex *idx = new MergedFileIndex(run.runNumber());
            idx->m_status = static_cast<MergedFile::Status>(stmt.get<int32_t>());
            idx->m_size = stmt.get<int64_t>();
            idx->m_retries = stmt.get<int64_t>();
            idx->m_retryDate = stmt.get<int64_t>();
            if (idx->m_retryDate == 0)
                idx->m_retryDate = -1;
            index = makeShared(idx);
        }
        else
        {
            MergedFile *file = new MergedFile(run.runNumber(), fileNumber);
            file->m_status = static_cast<MergedFile::Status>(
                stmt.get<int32_t>());
            file->m_size = stmt.get<int64_t>();
            file->m_retries = stmt.get<int64_t>();
            file->m_retryDate = stmt.get<int64_t>();
            if (file->m_retryDate == 0)
                file->m_retryDate = -1;
            out.push_back(makeShared(file));
        }
    }
    return check(rc, "failed to select files");
}

bool RFMDatabase::update(MergedFile &file)
{
    int rc;
    StmtGuard stmt(m_stmts[FileUpdate]);

    if (!stmt)
    {
        rc = sqlite3_prepare_v2(m_db,
                                "UPDATE files SET status = ?, size = ? "
                                "WHERE runNumber = ? AND fileNumber = ?;",
                                -1, &stmt.stmt, nullptr);
        if (check(rc, "failed to prepare files UPDATE statement"))
            m_stmts[FileUpdate] = stmt.stmt;
        else
            return false;
    }

    uint32_t retries;
    MergedFile::Status status;
    std::time_t retryDate(-1);

    file.m_lock.lock();
    status = file.m_status;
    retries = file.m_retries;
    retryDate = file.m_retryDate;

    stmt.bind(status);
    stmt.bind(file.m_size);
    stmt.bind(file.m_runNumber);
    stmt.bind(file.m_fileNumber);
    file.m_storeTask.reset();
    file.m_lock.unlock();

    rc = stmt.step();
    if (retries > 0)
        return check(rc, "failed to update file") &&
            updateRetries(file.runNumber(), file.fileNumber(), retries,
                          retryDate);
    else
        return check(rc, "failed to update file");
}

bool RFMDatabase::updateRetries(RunInfo::RunNumber runNumber,
                                MergedFile::FileNumber fileNumber,
                                uint32_t retries,
                                std::time_t retryDate)
{
    int rc;
    StmtGuard stmt(m_stmts[FileRetriesUpdate]);

    if (!stmt)
    {
        rc = sqlite3_prepare_v2(m_db,
                                "INSERT OR REPLACE INTO fileRetries"
                                "(runNumber,fileNumber,retries,retryDate) "
                                "VALUES(?,?,?,?);",
                                -1, &stmt.stmt, nullptr);
        if (check(rc, "failed to prepare fileRetries UPDATE statement"))
            m_stmts[FileRetriesUpdate] = stmt.stmt;
        else
            return false;
    }

    stmt.bind(runNumber);
    stmt.bind(fileNumber);
    stmt.bind(retries);
    stmt.bind(retryDate);

    rc = stmt.step();
    return check(rc, "failed to update fileRetries");
}

FileEvent::Shared RFMDatabase::insertFileEvent(const MergedFile &file,
                                               FileEvent::SequenceNumber seqNum,
                                               FileEvent::EventNumber eventNumber,
                                               std::size_t size,
                                               std::size_t offset)
{
    int rc;
    StmtGuard stmt(m_stmts[EventInsert]);

    if (!stmt)
    {
        rc = sqlite3_prepare_v2(
            m_db,
            "INSERT INTO events "
            "(runNumber,fileNumber,seqNum,event,size,offset) VALUES "
            "(?,?,?,?,?,?);",
            -1, &stmt.stmt, nullptr);
        if (check(rc, "failed to prepare events INSERT statement"))
            m_stmts[EventInsert] = stmt.stmt;
        else
            return FileEvent::Shared();
    }

    stmt.bind(file.runNumber());
    stmt.bind(file.fileNumber());
    stmt.bind(seqNum);
    stmt.bind(eventNumber);
    stmt.bind(size);
    stmt.bind(offset);

    rc = stmt.step();
    if (!check(rc, "failed to insert event"))
        return FileEvent::Shared();

    return makeShared(new FileEvent(file, seqNum, eventNumber, size, offset));
}

bool RFMDatabase::getFileEvents(const MergedFile &file,
                                std::vector<FileEvent::Shared> &out,
                                std::size_t &totalSize)
{
    int rc;
    StmtGuard stmt(m_stmts[EventGet]);

    if (!stmt)
    {
        rc = sqlite3_prepare_v2(m_db,
                                "SELECT seqNum,event,size,offset "
                                "FROM events "
                                "WHERE runNumber = ? AND FileNumber = ? "
                                "ORDER BY seqNum ASC;",
                                -1, &stmt.stmt, nullptr);
        if (check(rc, "failed to prepare events SELECT statement"))
            m_stmts[EventGet] = stmt.stmt;
        else
            return false;
    }

    out.clear();
    totalSize = 0;
    stmt.bind(file.runNumber());
    stmt.bind(file.fileNumber());
    for (rc = stmt.step(); rc == SQLITE_ROW; rc = stmt.step())
    {
        const FileEvent::SequenceNumber seqNum = stmt.get<int64_t>();
        const FileEvent::EventNumber eventNum = stmt.get<int64_t>();
        const std::size_t size = stmt.get<int64_t>();
        const std::size_t offset = stmt.get<int64_t>();
        out.push_back(
            makeShared(new FileEvent(file, seqNum, eventNum, size, offset)));
        totalSize += size;
    }

    return check(rc, "failed to select events");
}

bool RFMDatabase::update(FileEvent &event)
{
    int rc;
    StmtGuard stmt(m_stmts[EventUpdate]);

    if (!stmt)
    {
        rc = sqlite3_prepare_v2(
            m_db,
            "UPDATE events SET offset = ? "
            "WHERE runNumber = ? AND fileNumber = ? AND seqNum = ?;",
            -1, &stmt.stmt, nullptr);
        if (check(rc, "failed to prepare files UPDATE statement"))
            m_stmts[EventUpdate] = stmt.stmt;
        else
            return false;
    }

    event.m_lock.lock();
    stmt.bind(event.m_offset);
    stmt.bind(event.m_runNumber);
    stmt.bind(event.m_fileNumber);
    stmt.bind(event.m_seqNum);
    event.m_storeTask.reset();
    event.m_lock.unlock();

    rc = stmt.step();
    return check(rc, "failed to update event");
}

bool RFMDatabase::getFileStats(FileStats &stats)
{
    int rc;
    StmtGuard stmt(m_stmts[FileGetStats]);

    if (!stmt)
    {
        rc = sqlite3_prepare_v2(m_db,
                                "SELECT status,COUNT(status) "
                                "FROM files "
                                "GROUP BY status;",
                                -1, &stmt.stmt, nullptr);
        if (check(rc, "failed to prepare file SELECT (stats) statement"))
            m_stmts[FileGetStats] = stmt.stmt;
        else
            return false;
    }

    stats.clear();
    for (rc = stmt.step(); rc == SQLITE_ROW; rc = stmt.step())
    {
        MergedFile::Status status = static_cast<MergedFile::Status>(
            stmt.get<int32_t>());
        stats[status] = stmt.get<int64_t>();
    }
    return check(rc, "failed to select file stats");
}

bool RFMDatabase::getNextRetry(RunInfo::RunNumber &runNumber,
                               MergedFile::FileNumber &fileNumber,
                               std::time_t &date)
{
    int rc;
    StmtGuard stmt(m_stmts[FileGetNextRetry]);

    if (!stmt)
    {
        rc = sqlite3_prepare_v2(m_db,
                                "SELECT runNumber,fileNumber,retryDate "
                                "FROM fileRetries WHERE retryDate > 0 "
                                "ORDER BY retryDate ASC LIMIT 1;",
                                -1, &stmt.stmt, nullptr);
        if (check(rc, "failed to prepare file SELECT (retry) statement"))
            m_stmts[FileGetNextRetry] = stmt.stmt;
        else
            return false;
    }

    runNumber = RunInfo::InvalidRunNumber;
    fileNumber = MergedFile::InvalidFileNumber;
    date = std::time_t(-1);
    for (rc = stmt.step(); rc == SQLITE_ROW; rc = stmt.step())
    {
        runNumber = static_cast<RunInfo::RunNumber>(stmt.get<int64_t>());
        fileNumber = static_cast<MergedFile::FileNumber>(stmt.get<int64_t>());
        date = stmt.get<std::time_t>();
    }
    return check(rc, "failed to select file next retry");
}

bool RFMDatabase::getNextExpiry(RunInfo::RunNumber &runNumber, std::time_t &date)
{
    int rc;
    StmtGuard stmt(m_stmts[RunGetNextExpiry]);

    if (!stmt)
    {
        rc = sqlite3_prepare_v2(m_db,
                                "SELECT runNumber,expiryDate "
                                "FROM runs WHERE expiryDate > 0 "
                                "ORDER BY expiryDate ASC LIMIT 1;",
                                -1, &stmt.stmt, nullptr);
        if (check(rc, "failed to prepare file SELECT (expiry) statement"))
            m_stmts[RunGetNextExpiry] = stmt.stmt;
        else
            return false;
    }

    runNumber = RunInfo::InvalidRunNumber;
    date = std::time_t(-1);
    for (rc = stmt.step(); rc == SQLITE_ROW; rc = stmt.step())
    {
        runNumber = static_cast<RunInfo::RunNumber>(stmt.get<int64_t>());
        date = stmt.get<std::time_t>();
    }
    return check(rc, "failed to select run next expiry");
}

bool RFMDatabase::check(int rc, const char *msg, char *sqliteMsg)
{
    switch (rc)
    {
    case SQLITE_OK:
    case SQLITE_DONE:
    case SQLITE_ROW: return true;
    default:
    {
        const char *details = sqliteMsg ? sqliteMsg : sqlite3_errmsg(m_db);
        m_errorString = msg;
        LOG(ERROR) << "[RFMDatabase] " << msg << ": " << details;

        if (sqliteMsg)
            sqlite3_free(sqliteMsg);
        return false;
    }
    }
}
