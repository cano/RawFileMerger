/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-10T13:15:08+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef FAKEARCHIVER_HPP__
#define FAKEARCHIVER_HPP__

#include <cstdint>
#include <map>
#include <mutex>
#include <string>

#include "Archiver.hpp"

namespace ntof {
namespace rfm {

class FakeArchiver : public Archiver
{
public:
    typedef std::map<boost::filesystem::path, uint64_t> FakeFiles;

    FakeArchiver() = default;

    const std::string &name() const override;

    File::Shared open(const boost::filesystem::path &file) override;

    bool isMigrated(const boost::filesystem::path &file) override;

    FileInfo getInfo(const boost::filesystem::path &file) override;

    void createDir(const boost::filesystem::path &file) override;

    void deleteFile(const boost::filesystem::path &file) override;

    void renameFile(const boost::filesystem::path &file,
                    const boost::filesystem::path &newFile) override;

    FakeFiles files() const;
    void addFakeFile(const boost::filesystem::path &path, uint64_t size);

    static const std::string NAME;

protected:
    FakeFiles m_vfs;
    mutable std::mutex m_lock;
};

} // namespace rfm
} // namespace ntof

#endif
