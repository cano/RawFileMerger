/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-10T13:32:18+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "CastorArchiver.hpp"

#include <mutex>
#include <sstream>

#include <easylogging++.h>
#include <shift/Cns_api.h>
#include <shift/serrno.h>
#include <shift/stager_client_api.h>

#include "Config.h"

#include <xrootd/XrdCl/XrdClFile.hh>
#include <xrootd/XrdCl/XrdClFileSystem.hh>

namespace bfs = boost::filesystem;
using namespace ntof::rfm;

const std::string CastorArchiver::NAME("CastorArchiver");
static std::mutex s_lock;

class CastorFile : public Archiver::File
{
public:
    explicit CastorFile(const std::string &filename);
    ~CastorFile();

    void write(uint64_t offset, uint32_t size, const void *buffer) override;

    XrdCl::File m_file;
};

CastorFile::CastorFile(const std::string &filename)
{
    Config &c = Config::instance();
    std::ostringstream oss;
    oss << "root://" << c.getCastorNode() << "/" << filename
        << "?svcClass=" << c.getSvcClass();

    const std::string url = oss.str();

    LOG(TRACE) << "[CastorArchiver]: Opening XrdCl::File at " << url;

    /* it seems that we're only checking write return code */
    m_file.Open(url, XrdCl::OpenFlags::Write | XrdCl::OpenFlags::Delete);
}

CastorFile::~CastorFile()
{
    m_file.Close();
}

void CastorFile::write(uint64_t offset, uint32_t size, const void *buffer)
{
    XrdCl::XRootDStatus s = m_file.Write(offset, size, buffer);
    if (!s.IsOK())
    {
        std::ostringstream oss;
        oss << "Unable to write to CASTOR " << s.GetErrorMessage();
        LOG(ERROR) << "[CastorArchiver]: " << oss.str();
        throw Archiver::Exception(oss.str());
    }
}

const std::string &CastorArchiver::name() const
{
    return CastorArchiver::NAME;
}

CastorArchiver::File::Shared CastorArchiver::open(const bfs::path &file)
{
    return CastorArchiver::File::Shared(new CastorFile(file.string()));
}

bool _isMigrated(const std::string &castorPath, struct stage_options &opts)
{
    bool isMigrated = false;
    struct stage_filequery_resp *resp;
    struct stage_query_req requests[1];
    int nbresps;

    requests[0].type = BY_FILENAME;
    requests[0].param = const_cast<char *>(castorPath.c_str());

    std::lock_guard<std::mutex> lock(s_lock);

    if (stage_filequery(requests, 1, &resp, &nbresps, &opts) == 0)
    {
        if (resp[0].errorCode == 0)
        {
            isMigrated = (resp[0].status == FILE_STAGEIN ||
                          resp[0].status == FILE_STAGED);
        }
        else
        {
            LOG(ERROR)
                << "[CastorArchiver]: unable to query stage status for file "
                << castorPath << " using SVC class " << opts.service_class
                << " : " << resp[0].errorMessage;
        }
        free_filequery_resp(resp, nbresps);
    }
    return isMigrated;
}

bool CastorArchiver::isMigrated(const bfs::path &file)
{
    bool isMigrated = false;
    std::string castorPath = file.string();

    struct stage_options Opts;

    Opts.stage_host = const_cast<char *>(
        Config::instance().getCastorNode().c_str());
    Opts.service_class = const_cast<char *>(
        Config::instance().getSvcClass().c_str());
    Opts.stage_version = 2;
    Opts.stage_port = 9002;

    LOG(DEBUG) << "[CastorArchiver]: testing stager state for " << castorPath;
    isMigrated = _isMigrated(castorPath, Opts);
    if (!isMigrated)
    {
        Opts.service_class = const_cast<char *>("*");
        isMigrated = _isMigrated(castorPath, Opts);
    }

    if (isMigrated)
    {
        LOG(DEBUG) << "[CastorArchiver]: file " << castorPath << " is migrated";
    }
    return isMigrated;
}

CastorArchiver::FileInfo CastorArchiver::getInfo(const bfs::path &file)
{
    std::string path = file.string();
    struct Cns_filestat fileStat;

    if (path[path.length() - 1] == '/')
    {
        path = path.substr(0, path.length() - 1);
    }
    std::lock_guard<std::mutex> lock(s_lock);
    LOG(TRACE)
        << "[CastorArchiver]: getting CASTOR file information for " << path;
    if (Cns_lstat(path.c_str(), &fileStat) < 0)
        return CastorArchiver::FileInfo();
    else
        return CastorArchiver::FileInfo(true, fileStat.filesize);
}

void CastorArchiver::createDir(const bfs::path &file)
{
    struct Cns_filestat statbuf;
    mode_t mask = 0;

    std::string path = file.string();

    std::lock_guard<std::mutex> lock(s_lock);
    mask = Cns_umask(0);
    Cns_umask(mask);
    // Remove trailing /
    if (!path.empty() && path[path.length() - 1] == '/')
    {
        path = path.substr(0, path.length() - 1);
    }

    if (path.empty() ||
        ((path[0] != '/') && (path.find(":/") == std::string::npos)))
    {
        std::ostringstream oss;
        oss << "cannot create directory " << path << ": invalid path";
        LOG(ERROR) << "[CastorArchiver]: " << oss.str();
        throw Archiver::Exception(oss.str());
    }

    // Check if directory exists
    if (Cns_lstat(path.c_str(), &statbuf) == 0)
    {
        if ((statbuf.filemode & S_IFDIR) == 0)
        {
            // It's NOT a folder
            std::ostringstream oss;
            oss << "cannot create directory " << path
                << ": path already exists!";
            LOG(ERROR) << "[CastorArchiver]: " << oss.str();
            throw Archiver::Exception(oss.str());
        }
        else
        {
            LOG(TRACE)
                << "[CastorArchiver]: folder " << path << " already exists";
            // already exists and is a folder
            return;
        }
    }

    std::string::size_type parLoc;

    // Gets first existing parent directory on CASTOR
    std::string parent = path;
    do
    {
        parLoc = parent.rfind('/');
        parent = parent.substr(0, parLoc);
        if (Cns_access(parent.c_str(), F_OK) == 0)
            break;
    } while (parLoc > 0);

    /* make sure that mask allow write/execute for owner */
    if (mask & 0300)
    {
        Cns_umask(mask & ~0300);
    }

    // Create parents folders
    do
    {
        parLoc = path.find_first_of('/', parLoc + 1);
        std::string newPath = path.substr(0, parLoc);
        if ((parLoc == std::string::npos) && (mask & 0300))
        {
            Cns_umask(mask);
        }
        LOG(TRACE) << "[CastorArchiver]: creating CASTOR directory " << newPath;
        int c = Cns_mkdir(newPath.c_str(), 0777);
        if (c < 0 && serrno != EEXIST)
        {
            std::ostringstream oss;
            oss << "cannot create directory " << path << ": "
                << sstrerror(serrno);
            LOG(ERROR) << "[CastorArchiver]: " << oss.str();
            throw Archiver::Exception(oss.str());
        }
    } while (parLoc != std::string::npos);
}

void CastorArchiver::deleteFile(const bfs::path &file)
{
    int ret = 0;
    std::string filePath = file.string();
    struct Cns_filestat statbuf;

    std::lock_guard<std::mutex> lock(s_lock);
    LOG(TRACE) << "[CastorArchiver]: deleting CASTOR file at " << filePath;
    if ((ret = Cns_lstat(filePath.c_str(), &statbuf)) == 0)
    {
        if ((statbuf.filemode & S_IFDIR) == 0)
        {
            // Not a directory
            ret = Cns_unlink(filePath.c_str());
        }
        else
        {
            std::ostringstream oss;
            oss << "CASTOR path " << filePath << " is a directory!";
            LOG(ERROR) << "[CastorArchiver]: " << oss.str();
            throw Archiver::Exception(oss.str());
        }
    }
    if (ret != 0)
    {
        std::ostringstream oss;
        oss << "Unable to delete CASTOR file " << filePath << ": "
            << sstrerror(ret);
        LOG(ERROR) << "[CastorArchiver]: " << oss.str();
        throw Archiver::Exception(oss.str());
    }
}

void CastorArchiver::renameFile(const bfs::path &file, const bfs::path &newFile)
{
    const std::string oldPath = file.string();
    const std::string newPath = newFile.string();

    LOG(TRACE) << "[CastorArchiver]: renaming CASTOR file from " << oldPath
               << " to " << newPath;
    std::lock_guard<std::mutex> lock(s_lock);
    if (Cns_rename(oldPath.c_str(), newPath.c_str()) < 0)
    {
        std::ostringstream oss;
        oss << "Unable to rename CASTOR file " << oldPath << " to " << newPath
            << ":" << sstrerror(serrno);
        LOG(ERROR) << "[CastorArchiver]: " << oss.str();
        throw Archiver::Exception(oss.str());
    }
}
