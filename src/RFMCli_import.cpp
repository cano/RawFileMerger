/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-04-08T08:32:34+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <algorithm>
#include <fstream>
#include <string>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include <DaqTypes.h>
#include <NTOFLogging.hpp>

#include "RFMCli.hpp"
#include "RFMException.hpp"
#include "data/DaqInfo.hpp"
#include "data/RunInfo.hpp"

typedef std::vector<std::string> VArgs;

namespace po = boost::program_options;
namespace bfs = boost::filesystem;
using namespace ntof::rfm;
using namespace ntof::log;
using namespace ntof::dim;

void loadRunInfo(const bfs::path &runFile,
                 RunInfo::RunNumber &runNumber,
                 DaqInfo::CrateIdList &crates)
{
    std::ifstream ifs(runFile.string().c_str());
    if (!ifs.is_open() || !ifs.good())
        throw RFMException("Failed to open file");

    RunControlHeader rctr;
    ifs >> rctr;
    if (!ifs.good())
        throw RFMException("Failed to read RCTR at " +
                           std::to_string(ifs.tellg()));
    runNumber = rctr.runNumber;

    crates.clear();
    HeaderLookup lookup;
    while (ifs >> lookup)
    {
        ModuleHeader modh;
        ifs >> modh;
        if (!ifs.good() || (modh.size() == 0))
            throw RFMException("Failed to read MODH at " +
                               std::to_string(ifs.tellg()));
        crates.insert(modh.channelsConfig[0].getChassis());
    }
}

void loadEvent(const bfs::path &eventFile,
               FileEvent::EventNumber &event,
               std::size_t &size)
{
    EventHeader eveh;
    std::ifstream ifs(eventFile.string().c_str());
    if (!ifs.is_open() || !ifs.good())
        throw RFMException("Failed to open file");
    ifs >> eveh;
    if (!ifs.good())
        throw RFMException("Failed to load EVEH");

    event = eveh.eventNumber;
    size = eveh.sizeOfEvent * 4;
}

int import(const std::string &name, VArgs &vargs)
{
    bfs::path runFile;
    std::string experiment;

    po::options_description desc("Import Options");
    // clang-format off
    desc.add_options()
    ("help", "produce this help message")
    ("experiment", po::value(&experiment), "set experiment")
    ("approve", "Approve the run")
    ("runFile", po::value(&runFile)->required(), "Run file path");
    // clang-format on

    po::variables_map vm;
    po::store(po::command_line_parser(vargs).options(desc).run(), vm);

    if (vm.count("help"))
    {
        std::cout << "Usage: " << name << " ignoreFile [options]\n\n"
                  << desc << std::endl;
        po::notify(vm);
        return 0;
    }
    po::notify(vm);

    bool approve = (vm.count("approve") != 0);
    bfs::path runDir = runFile.parent_path();
    RunInfo::RunNumber runNumber;
    DaqInfo::CrateIdList crates;

    loadRunInfo(runFile, runNumber, crates);
    // if there are raw files, then we're standalone
    bool isStandalone = std::any_of(bfs::directory_iterator(runDir),
                                    bfs::directory_iterator(),
                                    [](const bfs::directory_entry &e) {
                                        return e.path().extension() == ".raw";
                                    });

    RFMCli cli;
    std::cout << "Creating " << (isStandalone ? "managed" : "standalone")
              << " run " << runNumber << std::endl;
    if (!cli.cmdStart(runNumber, crates, experiment, &approve, &isStandalone))
        throw RFMException("Failed to create run");

    std::map<FileEvent::EventNumber, std::size_t> eventMap;
    for (bfs::directory_iterator it(runDir); it != bfs::directory_iterator();
         ++it)
    {
        const bfs::path &eventFile = it->path();
        if (eventFile.extension() != ".event")
            continue;

        FileEvent::EventNumber eventNumber;
        std::size_t size;
        loadEvent(eventFile, eventNumber, size);
        eventMap[eventNumber] = size;
    }

    for (std::map<FileEvent::EventNumber, std::size_t>::value_type &val :
         eventMap)
    {
        std::cout << "Adding event " << val.first << std::endl;
        if (!cli.cmdEvent(runNumber,
                          static_cast<FileEvent::SequenceNumber>(val.first),
                          val.first, val.second))
            throw RFMException("Failed to create event");
    }
    if (!cli.cmdStop(runNumber))
        throw RFMException("Failed to stop run");
    return 0;
}
