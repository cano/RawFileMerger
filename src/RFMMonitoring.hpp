/*
 * RFMMonitoring.hpp
 *
 *  Created on: Jan 15, 2016
 *      Author: mdonze
 */

#ifndef RFMONITORING_HPP__
#define RFMONITORING_HPP__

#include <chrono>
#include <condition_variable>
#include <map>
#include <memory>
#include <mutex>
#include <set>
#include <string>
#include <thread>
#include <vector>

#include <DIMDataSet.h>
#include <DIMParamList.h>
#include <DIMState.h>
#include <DIMXMLService.h>

#include "RFMMonitoringHandlers.hpp"
#include "RFMUtils.hpp"
#include "data/MergedFile.hpp"
#include "data/RunInfo.hpp"

namespace ntof {
namespace rfm {

/**
 * Class used to monitor merger activity
 */
class RFMMonitoring : public DimErrorHandler
{
public:
    RFMMonitoring();
    virtual ~RFMMonitoring();

    enum RunsParams
    {
        RP_RUN_NUMBER = 0,
        RP_EXPERIMENT,
        RP_APPROVED,
        RP_STARTDATE,
        RP_STOPDATE,
        RP_EXPIRYDATE,
        RP_TRANSFERRED,
        RP_TRANSFERRATE,
        RP_STATS
    };

    enum CurrentRunParams
    {
        CRP_RUN_NUMBER = 1,
        CRP_EXPERIMENT,
        CRP_APPROVED,
        CRP_STARTDATE,
        CRP_STOPDATE,
        CRP_EXPIRYDATE,
        CRP_TRANSFERRED,
        CRP_TRANSFERRATE,
        CRP_STATS
    };

    enum FilesParams
    {
        FP_RUN_NUMBER = 0,
        FP_FILES,
        FP_INDEX
    };

    enum FileInfoParams
    {
        FIP_STATUS = 0,
        FIP_IGNORED = 1,
        FIP_SIZE = 2,
        FIP_TRANSFERRED = 3,
        FIP_RATE = 4,
        FIP_FILENAME = 5 /* index only */
    };

    /**
     * @brief set main run to monitor
     */
    void setCurrentRun(RunInfo &run);

    /**
     * @brief retrieve current run being monitored
     */
    RunInfo::Shared currentRun() const;

    /**
     * @brief notify the service that something has changed on the given Run
     */
    void notify(RunInfo::RunNumber run);

    /**
     * @brief notify the service that something has changed on the given file
     */
    void notify(RunInfo::RunNumber run, MergedFile::FileNumber file);

    /**
     * Sets a warning to be published
     * @param ident Warning identifier (usually CASTOR file name)
     * @param msg Message to be set
     * @details set an empty message to clear a warning
     */
    void setWarning(const std::string &ident,
                    const std::string &msg = std::string());

    /**
     * Sets a error to be published
     * @param ident Error identifier (usually CASTOR file name)
     * @param msg Message to be set
     * @details set an empty message to clear an error
     */
    void setError(const std::string &ident,
                  const std::string &msg = std::string());

    /**
     * @brief start the monitoring thread
     */
    void start();

    /**
     * @brief stop the monitoring thread
     */
    void stop();

    /**
     * @brief modify the monitoring interval
     * @param ms monitoring loop interval in miliseconds
     */
    void setInterval(uint32_t ms);

    /**
     * @brief update service information
     * @details this is automatically called when the thread is started
     */
    void update();

    /**
     * @brief convert a file to DIMData object
     * @param[in]  file file to convert
     * @param[in]  index whereas the file is an index
     * @return nested DIMData object
     */
    static ntof::dim::DIMData toData(const MergedFile &file, bool index = false);

protected:
    typedef std::set<RunInfo::RunNumber> UpdatedRunsList;
    typedef std::set<MergedFile::FileNumber> UpdatedFilesList;
    typedef std::map<std::string, int32_t> ErrorMap;
    typedef std::set<int32_t> CodesList;
    typedef std::map<RunInfo::RunNumber, TransferRate> RunTransferRateMap;
    typedef std::map<MergedFile::FileNumber, TransferRate> FileTransferRateMap;

    void thread_func();

    void updateFiles();
    void updateRuns();
    void updateCurrentRun(const ntof::dim::DIMData::List &dataset);
    void updateInfo();

    ntof::dim::DIMData toLiveData(const MergedFile &file, bool index = false);
    ntof::dim::DIMData &findRun(RunInfo::RunNumber runNumber);

    RunInfo::Shared m_run; /*<! current run */
    UpdatedRunsList m_updatedRuns;
    UpdatedFilesList m_updatedFiles;
    FileTransferRateMap m_fileRates;
    RunTransferRateMap m_runRates; // only accessed by monitoring thread
    TransferRate m_rate;           // only accessed by monitoring thread

    std::unique_ptr<std::thread> m_thread;
    bool m_started;
    mutable std::mutex m_lock;
    std::condition_variable m_cond;
    uint32_t m_interval;

    ntof::dim::DIMDataSet m_info;
    ntof::dim::DIMParamList m_runs;
    ntof::dim::DIMParamList m_current;
    ntof::dim::DIMDataSet m_currentFiles;
    FilesRpc m_files;

    ntof::dim::DIMState m_state;

    RunsParamListHandler m_runsHdlr;
    CurrentRunParamListHandler m_currentHdlr;

    uint32_t m_maxErrorsWarns; //!< Maximum errors/warnings
    ErrorMap m_warnings;       //!< List of active warnings
    ErrorMap m_errors;         //!< List of active error
    CodesList m_codes;
    int32_t m_lastCode;

    /**
     * Gets the next error/warning code
     * @param map Map of warning or error
     * @param code Code to be set
     * @return True if the limit is not reached
     */
    bool getNextCode(ErrorMap &map, int32_t &code);

    /**
     * DIM Error handler
     * @param severity
     * @param code
     * @param msg
     */
    void errorHandler(int severity, int code, char *msg);
};

} /* namespace rfm */
} /* namespace ntof */

#endif /* RFMONITORING_HPP__ */
