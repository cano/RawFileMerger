/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-04-14T17:10:58+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "RFMDaqListener.hpp"

#include <NTOFException.h>
#include <easylogging++.h>

#include "RFMFactory.hpp"
#include "RFMManager.hpp"
#include "data/DaqInfo.hpp"
#include "data/RunInfo.hpp"

using namespace ntof::rfm;
using namespace ntof::dim;

#define NTOF_THROW(msg, code) \
    throw ntof::NTOFException(msg, __FILE__, __LINE__, code)

RFMDaqListener::RFMDaqListener(RFMManager &manager) : m_mgr(manager)
{
    listen();
}

RFMDaqListener::~RFMDaqListener()
{
    for (std::shared_ptr<DIMXMLInfo> &info : m_info)
        info->unsubscribe();
}

void RFMDaqListener::listen()
{
    LOG(INFO) << "[RFMDaqListener]: starting to listen";

    const RFMFactory::DaqInfoList &list(RFMFactory::instance().getDaqInfoList());
    for (const DaqInfo::Shared &daq : list)
    {
        DaqInfo::CrateId id = daq->crateId();
        RFMManager &manager = m_mgr;

        std::shared_ptr<DIMXMLInfo> info(new DIMXMLInfo());
        info->dataSignal.connect([id, &manager](pugi::xml_document &doc,
                                                DIMXMLInfo &) {
            try
            {
                pugi::xml_node node = doc.child("RunExtensionNumberWrote");
                RunInfo::RunNumber runNumber = node.attribute("run").as_uint(
                    RunInfo::InvalidRunNumber);
                if (runNumber == RunInfo::InvalidRunNumber)
                    NTOF_THROW("Invalid run", 1);
                int32_t event = node.attribute("event").as_int(-1);

                if (event == -1)
                {
                    LOG(DEBUG)
                        << "[RFMDaqListener]: not an event (crateId:" << id
                        << "): -1";
                    return;
                }
                else if (event < 0)
                    NTOF_THROW("Invalid event: " + std::to_string(event), 1);

                uint64_t fileSize = node.attribute("size").as_ullong(0);

                if (!manager.addDaqEvent(runNumber, event, fileSize))
                    NTOF_THROW("Failed to add event", 1);
            }
            catch (std::exception &ex)
            {
                LOG(ERROR) << "[RFMDaqListener]: Exception (crateId:" << id
                           << "):" << ex.what();
            }
        });
        info->errorSignal.connect(
            [id](const std::string &message, DIMXMLInfo &) {
                LOG(ERROR) << "[RFMDaqListener]: Error (crateId:" << id
                           << "):" << message;
            });
        info->subscribe(daq->hostname() + "/WRITER/RunExtensionNumber");
        m_info.emplace_back(info);
    }
}
