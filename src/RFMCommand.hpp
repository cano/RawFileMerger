/*
 * RFMCommand.h
 *
 *  Created on: Jan 8, 2016
 *      Author: mdonze
 */

#ifndef RFMCOMMAND_HPP__
#define RFMCOMMAND_HPP__

#include <string>

#include <DIMSuperCommand.h>

namespace ntof {
namespace rfm {

class RFMManager;

class RFMCommand : public ntof::dim::DIMSuperCommand
{
public:
    explicit RFMCommand(RFMManager &mgr);
    virtual ~RFMCommand() = default;

protected:
    /**
     * Method called by the DIMSuperCommand when a client send a command
     * @param cmdData Command object containing command data
     */
    void commandReceived(ntof::dim::DIMCmd &cmdData) override;

    void cmdEvent(pugi::xml_node &node);
    void cmdRunStart(pugi::xml_node &node);
    void cmdRunStop(pugi::xml_node &node);
    void cmdFileIgnore(pugi::xml_node &node);

    RFMManager &m_mgr;
};

} // namespace rfm
} // namespace ntof

#endif /* RFMCOMMAND_HPP__ */
