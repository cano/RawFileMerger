//
// Created by matteof on 01/07/2020.
//

#include "Config.h"

#include <NTOFException.h>
#include <easylogging++.h>
#include <pugixml.hpp>

#include <Singleton.hxx>

using namespace ntof::rfm;

template class ntof::utils::Singleton<Config>;

const std::string Config::configFile = "/etc/ntof/rfmerger.xml";
const std::string Config::configMiscFile = "/etc/ntof/misc.xml";
const std::string Config::defaultDatabaseName = "/var/lib/RawFileMerger/"
                                                "db.sqlite";

Config::Config(const std::string &file, const std::string &miscFile) :
    ConfigMisc(miscFile),
    standaloneMode_(false),
    deleterEnabled_(true)
{
    pugi::xml_document doc;

    // Read the config file
    pugi::xml_parse_result result = doc.load_file(file.c_str());
    if (!result)
    {
        LOG(ERROR) << "Unable to open and parse configuration file!";
        throw NTOFException("Unable to open and parse configuration file!",
                            __FILE__, __LINE__);
    }

    // Parse the config file
    const pugi::xml_node &root = doc.first_child();

    // CASTOR paths
    const pugi::xml_node &paths = root.child("paths");
    // Load local file path (for run and event files)
    paths_.local = getPath("Local", paths.child("local"));
    // Load remote CASTOR file path
    paths_.remote = getPath("Remote", paths.child("remote"));
    // Load mount point
    paths_.mount = paths.child("mount").attribute("path").as_string("/mnt");
    paths_.database = paths.child("database")
                          .attribute("path")
                          .as_string(defaultDatabaseName.c_str());

    // Gets CASTOR SVC node and service class
    const pugi::xml_node &castor = root.child("castor");

    const pugi::xml_attribute &castorNodAttr = castor.attribute("node");
    if (castorNodAttr.empty())
    {
        throw NTOFException("CASTOR cluster node cannot be empty!", __FILE__,
                            __LINE__);
    }
    castorNode_ = castorNodAttr.as_string();

    const pugi::xml_attribute &castorSvcClassAttr = castor.attribute(
        "svcclass");
    if (castorSvcClassAttr)
    {
        svcClass_ = castorSvcClassAttr.as_string("default");
    }

    // CASTOR File limits to reach
    const pugi::xml_node &limitNode = root.child("limits");
    minTotalSize_ = parseSize(limitNode.attribute("size").as_string("2G"));
    maxEvents_ = limitNode.attribute("count").as_int(50);
    m_expiry = limitNode.attribute("expiry").as_int(60 * 60 * 24 *
                                                    31); // 31 days

    // DIM service prefix and history count
    const pugi::xml_node &dimNode = root.child("DIM");
    dimPrefix_ = dimNode.attribute("prefix").as_string("CASTOR");
    dimHistoryCount_ = dimNode.attribute("history").as_int(10);
    dimErrorsCount_ = dimNode.attribute("errors").as_uint(20);

    // Thread pool size
    poolSize_ = root.child("pool").attribute("size").as_int(20);
    // Buffer size
    bufferSize_ = root.child("buffer").attribute("size").as_ullong(0);

    archiverName_ =
        root.child("archiver").attribute("name").as_string("CastorArchiver");

    // CASTOR replace strategy
    std::string replaceStr =
        root.child("replace").attribute("strategy").as_string("");
    replaceSty_ = getReplaceStrategyFromString(replaceStr);
    if (replaceSty_ == UNKNOWN)
    {
        LOG(ERROR) << "Unsupported replace strategy name!";
        throw NTOFException("Replace strategy must be RENAME or DELETE!",
                            __FILE__, __LINE__);
    }

    int standalone = root.child("standalone").attribute("value").as_int();
    standaloneMode_ = (standalone > 0) ? true : false;
}

Config &Config::load(const std::string &file, const std::string &miscFile)
{
    ntof::utils::SingletonMutex lock;

    if (m_instance)
    {
        delete m_instance;
        m_instance = nullptr;
    }

    m_instance = new Config(file, miscFile);
    return *m_instance;
}

const std::string &Config::getDimPrefix() const
{
    return dimPrefix_;
}

uint32_t Config::getDimHistoryCount() const
{
    return dimHistoryCount_;
}

uint32_t Config::getDimErrorsCount() const
{
    return dimErrorsCount_;
}

const std::string &Config::getCastorNode() const
{
    return castorNode_;
}

const std::string &Config::getSvcClass() const
{
    return svcClass_;
}

uint32_t Config::getMaxEvents() const
{
    return maxEvents_;
}

uint64_t Config::getMinTotalSize() const
{
    return minTotalSize_;
}

Config::ReplaceStrategy Config::getReplaceStrategy() const
{
    return replaceSty_;
}

bool Config::isDeleterEnabled() const
{
    return deleterEnabled_;
}

void Config::setDeleterEnabled(bool value)
{
    deleterEnabled_ = value;
}

const std::string &Config::getArchiverName() const
{
    return archiverName_;
}

/**
 * Gets a string representation of the ReplaceStrategy
 * @return
 */
std::string Config::getReplaceStrategyAsString() const
{
    switch (replaceSty_)
    {
    case RENAME: return "RENAME";
    case DELETE: return "DELETE";
    default: return "UNKNOWN";
    }
}

/**
 * Gets a ReplaceStrategy enum from String
 * @return
 */
Config::ReplaceStrategy Config::getReplaceStrategyFromString(
    std::string &sty) const
{
    std::transform(sty.begin(), sty.end(), sty.begin(), ::toupper);
    if (sty == "RENAME")
    {
        return RENAME;
    }
    else if (sty == "DELETE")
    {
        return DELETE;
    }
    else
    {
        return UNKNOWN;
    }
}

const Config::Paths &Config::getPaths() const
{
    return paths_;
}

uint32_t Config::getPoolSize() const
{
    return poolSize_;
}
uint64_t Config::getBufferSize() const
{
    return bufferSize_;
}

/**
 * Helper function to extract the path attribute from a node
 * @param pathNode
 * @return
 */
std::string Config::getPath(const std::string &name,
                            const pugi::xml_node &pathNode) const
{
    if (pathNode)
    {
        std::string ret = pathNode.attribute("path").as_string();
        if (ret.empty())
        {
            std::ostringstream oss;
            oss << name << " path cannot be empty!";
            LOG(ERROR) << oss.str();
            throw NTOFException(oss.str(), __FILE__, __LINE__);
        }
        if (ret[ret.length() - 1] == '/')
        {
            ret = ret.substr(0, ret.length() - 1);
        }
        return ret;
    }
    else
    {
        std::ostringstream oss;
        oss << name << " path node is not valid!";
        LOG(ERROR) << oss.str();
        throw NTOFException(oss.str(), __FILE__, __LINE__);
    }
}

/**
 * Gets how the software will behave if CASTOR file is already present
 * @return
 */
/**
 * Parse a string giving a size
 * @param sizeStr
 * @return
 */
uint64_t Config::parseSize(const std::string &sizeStr) const
{
    int size = 0;
    char unit[64];
    int res = sscanf(sizeStr.c_str(), "%d%s", &size, unit);
    if (res == 1)
    {
        LOG(INFO) << "Size unit not specified, using bytes";
        // No unit specified, it's only bytes
        return ((uint64_t) size);
    }
    else
    {
        std::string unitStr = unit;
        switch (unitStr[0])
        {
        case 'K':
        case 'k':
            // This is kilobytes
            return ((uint64_t) size) * 1024;
        case 'M':
        case 'm':
            // This is megabytes
            return ((uint64_t) size) * 1048576;
        case 'G':
        case 'g':
            // This is gigabytes
            return ((uint64_t) size) * 1073741824;
        case 'T':
        case 't':
            // This is terabytes?
            return ((uint64_t) size) * 1099511627776U;
        default: return ((uint64_t) size);
        }
    }
}
