/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-04-07T09:25:35+02:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include <chrono>
#include <ctime>
#include <exception>

#include <boost/filesystem.hpp>

#include <DIMData.h>
#include <NTOFLogging.hpp>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <easylogging++.h>

#include "Archiver.hpp"
#include "Config.h"
#include "Env.hpp"
#include "Paths.hpp"
#include "RFMCli.hpp"
#include "RFMFactory.hpp"
#include "RFMManager.hpp"
#include "archiver/FakeArchiver.hpp"
#include "test_helpers.hpp"
#include "test_helpers_rfm.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;
using namespace ntof::log;
using ntof::dim::DIMData;

class TestRFMManagerOld : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRFMManagerOld);
    CPPUNIT_TEST(removeOld);
    CPPUNIT_TEST_SUITE_END();

    std::unique_ptr<DimTestHelper> m_dim;
    bfs::path m_path;

public:
    void setUp() override
    {
        m_path = envInit("TestRFMManagerOld");
        genFiles(m_path);
        m_dim.reset(new DimTestHelper());
    }

    void tearDown() override
    {
        m_dim.reset();
        envDestroy(m_path);
    }

    RunInfo::Shared createRun(RunInfo::RunNumber num, uint32_t files)
    {
        RFMFactory &fact = RFMFactory::instance();
        FakeArchiver *archiver = dynamic_cast<FakeArchiver *>(
            &Archiver::instance());
        DaqInfo::CrateIdList daqs;
        DaqInfo::fromString("1:2", daqs);

        ASSERT(archiver);
        // to have stopDate > startDate
        RunInfo::Shared run = fact.createRun(num, "EAR1",
                                             std::time(nullptr) - 1, daqs);
        EQ(true, bool(run));
        run->setApproved(true);
        for (uint32_t i = 0; i < files; ++i)
        {
            MergedFile::Shared file = run->addFile();
            if (i % 2)
                file->setStatus(MergedFile::IGNORED);
            else
            {
                file->setStatus(MergedFile::WAITING);
                file->setStatus(MergedFile::TRANSFERRING);
                file->setStatus(MergedFile::COPIED);
                file->setStatus(MergedFile::MIGRATED);
            }
            file->save();
            archiver->addFakeFile(Paths::getRemoteFilePath(*run, *file), 4096);
        }
        run->setStopDate(std::time(nullptr));
        {
            MergedFile::Shared file = run->createIndex();
            if (num % 2)
                file->setStatus(MergedFile::IGNORED);
            else
            {
                file->setStatus(MergedFile::WAITING);
                file->setStatus(MergedFile::TRANSFERRING);
                file->setStatus(MergedFile::COPIED);
                file->setStatus(MergedFile::MIGRATED);
            }
            file->save();
            archiver->addFakeFile(Paths::getRemoteFilePath(*run, *file), 4096);
        }
        run->save();
        return run;
    }

    void removeOld()
    {
        RFMFactory &factory = RFMFactory::instance();
        uint32_t max = Config::instance().getDimHistoryCount();

        ASSERT(max > 0);
        for (uint32_t i = 0; i < max + 10; ++i)
            createRun(100000 + i, 2);

        RFMFactory::RunNumberList old;
        EQ(true, factory.getMigratedRuns(old));
        EQ(std::size_t(max + 10), old.size());

        /* oldest run */
        EQ(true, bool(factory.getRun(100000)));

        RFMManager mgr;
        mgr.initialize();
        mgr.fileDeleter()->setInterval(100);
        mgr.monitor()->setInterval(100);
        mgr.start();

        RunInfoWaiter waiter;
        EQ(true,
           waiter.wait(
               [max](DIMData::List &data) { return data.size() == max + 1; },
               3000));
        EQ(false, bool(factory.getRun(100000)));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRFMManagerOld);
