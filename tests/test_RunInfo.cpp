/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-09T14:13:01+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <ctime>

#include <boost/filesystem.hpp>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Config.h"
#include "RFMFactory.hpp"
#include "data/DaqInfo.hpp"
#include "data/MergedFile.hpp"
#include "data/MergedFileIndex.hpp"
#include "data/RunInfo.hpp"
#include "test_helpers.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;

class TestRunInfo : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRunInfo);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(files);
    CPPUNIT_TEST(index);
    CPPUNIT_TEST_SUITE_END();

    bfs::path m_tmp;

public:
    void setUp()
    {
        m_tmp = bfs::temp_directory_path() / "TestRunInfo";
        bfs::create_directories(m_tmp);

        Config::Paths &paths = const_cast<Config::Paths &>(
            Config::instance().getPaths());
        paths.database = (m_tmp / "db.sql").string();
        RFMFactory::destroy(); /* force delete to re-create with new db path */
    }

    void tearDown()
    {
        RFMFactory::destroy(); /* force delete, db will be erased */
        bfs::remove_all(m_tmp);
    }

    void simple()
    {
        RFMFactory &factory = RFMFactory::instance();

        /* not in db */
        EQ(false, bool(factory.getRun(1234, true)));

        RunInfo::Shared run = factory.createRun(1234, "EAR1", 123,
                                                DaqInfo::CrateIdList());
        EQ(true, bool(run));
        EQ(RunInfo::RunNumber(1234), run->runNumber());
        EQ(std::string("EAR1"), run->expArea());

        /* in cache */
        EQ(true, bool(factory.getRun(1234, false)));
        ASSERT(factory.getRun(1234, false).get() == run.get());

        run.reset();
        EQ(false, bool(factory.getRun(1234, false)));
        run = factory.getRun(1234);
        EQ(true, bool(run));
        EQ(false, run->hasStopDate());

        run->setExperiment("myExp");
        run->setStopDate(std::time(nullptr));
        run->save();

        run.reset(); // fetch back from db
        run = factory.getRun(1234);
        EQ(true, bool(run));
        EQ(std::string("myExp"), run->experiment());
        EQ(true, run->hasStopDate());
    }

    void files()
    {
        RFMFactory &factory = RFMFactory::instance();

        RunInfo::Shared run = factory.createRun(1234, "EAR1", 123,
                                                DaqInfo::CrateIdList());
        EQ(true, bool(run));

        MergedFile::Shared file = run->addFile();
        EQ(true, bool(file));
        EQ(MergedFile::FileNumber(0), file->fileNumber());

        file = run->addFile();
        EQ(true, bool(file));
        EQ(MergedFile::FileNumber(1), file->fileNumber());

        run->setStopDate(std::time(nullptr));
        EQ(false, bool(run->addFile())); /* can't add file on stopped runs */
        run->save();

        run.reset();
        run = factory.getRun(1234);
        EQ(true, bool(run));
        EQ(std::size_t(2), run->getFiles().size());

        file = run->currentFile();
        EQ(true, bool(file));
        EQ(MergedFile::FileNumber(1), file->fileNumber());
    }

    void index()
    {
        RFMFactory &factory = RFMFactory::instance();

        RunInfo::Shared run = factory.createRun(1234, "EAR1", 123,
                                                DaqInfo::CrateIdList());
        EQ(true, bool(run));
        EQ(true, bool(run->addFile()));

        EQ(false,
           bool(run->createIndex())); /* can't add index on ongoing run */
        EQ(false, bool(run->index()));
        run->setStopDate(std::time(nullptr));

        MergedFileIndex::Shared index = run->createIndex();
        EQ(true, bool(index));
        ASSERT(run->createIndex() == index);

        EQ(true, bool(run->index()));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRunInfo);
