/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-23T10:17:23+01:00
**     Author: Sylvain Fargier <fargie_s> <sylvain.fargier@cern.ch>
**
*/

#include "test_helpers_rfm.hpp"

#include <chrono>

#include <easylogging++.h>

#include "RFMCli.hpp"
#include "test_helpers.hpp"

using ntof::dim::DIMData;
using ntof::rfm::RFMCli;
using namespace std::chrono;

bool RunInfoWaiter::wait(std::function<bool(DIMData::List &)> check,
                         unsigned int ms)
{
    RFMCli cli;
    SignalWaiter waiter;

    waiter.listen(cli.runsSignal);
    cli.listen();

    time_point<steady_clock> now, start = steady_clock::now();
    for (now = steady_clock::now();
         waiter.wait(1, ms - duration_cast<milliseconds>(now - start).count());
         now = steady_clock::now())
    {
        waiter.reset();
        DIMData::List info = cli.getRunsInfo();
        try
        {
            if (check(info))
                return true;
        }
        catch (std::exception &ex)
        {
            LOG(WARNING) << ex.what();
        }
    }
    return false;
}

bool CurrentRunWaiter::wait(std::function<bool(DIMData::List &)> check,
                            unsigned int ms)
{
    RFMCli cli;
    SignalWaiter waiter;

    waiter.listen(cli.currentSignal);
    cli.listen();

    time_point<steady_clock> now, start = steady_clock::now();
    for (now = steady_clock::now();
         waiter.wait(1, ms - duration_cast<milliseconds>(now - start).count());
         now = steady_clock::now())
    {
        waiter.reset();
        DIMData::List info = cli.getCurrentRun();
        try
        {
            if (check(info))
                return true;
        }
        catch (std::exception &ex)
        {
            LOG(WARNING) << ex.what();
        }
    }
    return false;
}

bool CurrentFilesWaiter::wait(std::function<bool(DIMData::List &)> check,
                              unsigned int ms)
{
    RFMCli cli;
    SignalWaiter waiter;

    waiter.listen(cli.filesSignal);
    cli.listen();

    time_point<steady_clock> now, start = steady_clock::now();
    for (now = steady_clock::now();
         waiter.wait(1, ms - duration_cast<milliseconds>(now - start).count());
         now = steady_clock::now())
    {
        waiter.reset();
        DIMData::List list = cli.getCurrentFiles();
        try
        {
            if (check(list))
                return true;
        }
        catch (std::exception &ex)
        {
            LOG(WARNING) << ex.what();
        }
    }
    return false;
}
