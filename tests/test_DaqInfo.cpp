/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-28T13:31:27+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "data/DaqInfo.hpp"
#include "test_helpers.hpp"

using namespace ntof::rfm;

class TestDaqInfo : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestDaqInfo);
    CPPUNIT_TEST(serialize);
    CPPUNIT_TEST_SUITE_END();

public:
    void serialize()
    {
        DaqInfo::CrateIdList list;
        EQ(true, DaqInfo::fromString("1:4:3", list));
        EQ(std::size_t(1), list.count(DaqInfo::CrateId(3)));
        EQ(std::size_t(3), list.size());

        std::string outStr;
        EQ(true, DaqInfo::toString(list, outStr));
        EQ(std::string("1:3:4"), outStr);

        EQ(false, DaqInfo::fromString("bad:string", list));
        EQ(false, DaqInfo::fromString(":", list));
        EQ(true, DaqInfo::fromString("", list));
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestDaqInfo);
