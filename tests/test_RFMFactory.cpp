/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-03-04T08:27:25+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <algorithm>
#include <ctime>

#include <boost/filesystem.hpp>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "Config.h"
#include "RFMFactory.hpp"
#include "data/DaqInfo.hpp"
#include "data/MergedFile.hpp"
#include "data/RunInfo.hpp"
#include "test_helpers.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;

class TestRFMFactory : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRFMFactory);
    CPPUNIT_TEST(daqInfo);
    CPPUNIT_TEST_SUITE_END();

    bfs::path m_tmp;

public:
    void setUp()
    {
        m_tmp = bfs::temp_directory_path() / "TestRFMFactory";
        bfs::create_directories(m_tmp);

        Config::Paths &paths = const_cast<Config::Paths &>(
            Config::instance().getPaths());
        paths.database = (m_tmp / "db.sql").string();
        RFMFactory::destroy(); /* force delete to re-create with new db path */
    }

    void tearDown()
    {
        RFMFactory::destroy(); /* force delete, db will be erased */
        bfs::remove_all(m_tmp);
    }

    void daqInfo()
    {
        /* relies on configMisc.xml */
        RFMFactory &factory = RFMFactory::instance();
        DaqInfo::Shared info = factory.getDaqInfo(1);
        EQ(true, bool(info));
        EQ(std::string("ntofdaq-m111"), info->name());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRFMFactory);
