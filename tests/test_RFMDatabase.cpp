/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2020-02-27T09:25:06+01:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <ctime>

#include <boost/filesystem.hpp>

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <easylogging++.h>

#include "Config.h"
#include "RFMDatabase.hpp"
#include "RFMFactory.hpp"
#include "data/DaqInfo.hpp"
#include "data/MergedFile.hpp"
#include "data/MergedFileIndex.hpp"
#include "data/RunInfo.hpp"
#include "test_helpers.hpp"

namespace bfs = boost::filesystem;
using namespace ntof::rfm;

class TestRFMDatabase : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestRFMDatabase);
    CPPUNIT_TEST(createDb);
    CPPUNIT_TEST(createDbError);
    CPPUNIT_TEST(runs);
    CPPUNIT_TEST(mergedFiles);
    CPPUNIT_TEST(mergedFileIndex);
    CPPUNIT_TEST(fileEvents);
    CPPUNIT_TEST(nextExpiry);
    CPPUNIT_TEST(nextRetry);
    CPPUNIT_TEST_SUITE_END();

    bfs::path m_tmp;

public:
    void setUp()
    {
        m_tmp = bfs::temp_directory_path() / "TestRFMDatabase";
        bfs::create_directories(m_tmp);

        Config::Paths &paths = const_cast<Config::Paths &>(
            Config::instance().getPaths());
        paths.database = (m_tmp / "test.sqlite").string();
        RFMFactory::destroy(); /* force delete to re-create with new db path */
    }

    void tearDown() { bfs::remove_all(m_tmp); }

    void createDb()
    {
        RFMDatabase db((m_tmp / "test.sqlite").string());
        EQ(std::string(), db.errorString());
    }

    void createDbError()
    {
        RFMDatabase db(m_tmp.string()); // a directory
        ASSERT(!db.errorString().empty());
    }

    void checkRun(const RunInfo::Shared &run,
                  std::time_t startDate,
                  std::time_t stopDate,
                  const std::string &exp)
    {
        EQ(true, bool(run));
        EQ(RunInfo::RunNumber(1234), run->runNumber());
        EQ(std::string("EAR1"), run->expArea());
        EQ(true, run->hasDaq(1));
        EQ(true, run->hasDaq(2));
        EQ(true, run->hasDaq(3));
        EQ(std::size_t(3), run->getDaqs().size());
        EQ(exp, run->experiment());
        EQ(startDate, run->startDate());
        EQ(stopDate, run->stopDate());
    }

    void runs()
    {
        RFMDatabase db((m_tmp / "test.sqlite").string());
        DaqInfo::CrateIdList daqs;
        RunInfo::Shared run;

        std::time_t startDate = std::time(nullptr);
        EQ(true, DaqInfo::fromString("1:2:3", daqs));
        run = db.insertRun(1234, "EAR1", startDate, daqs, "test");
        checkRun(run, startDate, std::time_t(-1), "test");

        run = db.getRun(1234);
        checkRun(run, startDate, std::time_t(-1), "test");

        std::time_t stopDate = startDate + 10;
        run->setExperiment("another test");
        run->setStopDate(stopDate);

        db.update(*run);
        run = db.getRun(1234);
        checkRun(run, startDate, stopDate, "another test");

        EQ(false,
           bool(db.insertRun(1234, "EAR1", startDate, daqs,
                             "test"))); /* already exist */
    }

    void mergedFiles()
    {
        RFMDatabase db((m_tmp / "test.sqlite").string());
        RunInfo::Shared run;
        MergedFile::Shared file;

        run = db.insertRun(1234, "EAR1", std::time(nullptr),
                           DaqInfo::CrateIdList());
        EQ(true, bool(run));

        file = db.insertMergedFile(*run, 42);
        EQ(true, bool(file));
        EQ(RunInfo::RunNumber(1234), file->runNumber());
        EQ(std::string("run1234_42_s1.raw.finished"), file->fileName());
        EQ(MergedFile::INCOMPLETE, file->status());
        EQ(MergedFile::FileNumber(42), file->fileNumber());

        db.insertMergedFile(*run, 1);
        EQ(false, bool(db.insertMergedFile(*run, 1))); /* already exist */
        db.insertMergedFile(*run, 2);

        std::vector<MergedFile::Shared> files;
        MergedFileIndex::Shared index;
        EQ(true, db.getMergedFiles(*run, files, index));
        EQ(std::size_t(3), files.size());
        EQ(MergedFile::FileNumber(1), files[0]->fileNumber());
        EQ(MergedFile::FileNumber(2), files[1]->fileNumber());
        EQ(MergedFile::FileNumber(42), files[2]->fileNumber());

        files[0]->setStatus(MergedFile::WAITING);
        EQ(true, db.update(*files[0]));
        EQ(true, db.getMergedFiles(*run, files, index));
        EQ(MergedFile::WAITING, files[0]->status());
    }

    void mergedFileIndex()
    {
        RFMDatabase db((m_tmp / "test.sqlite").string());
        RunInfo::Shared run;
        MergedFile::Shared file;

        run = db.insertRun(1234, "EAR1", std::time(nullptr),
                           DaqInfo::CrateIdList());
        EQ(true, bool(run));

        file = db.insertMergedFile(*run, MergedFile::IndexFileNumber);
        EQ(true, bool(file));
        EQ(RunInfo::RunNumber(1234), file->runNumber());
        EQ(std::string("run1234.idx.finished"), file->fileName());

        EQ(false,
           bool(db.insertMergedFile(
               *run, MergedFile::IndexFileNumber))); /* already exist */

        std::vector<MergedFile::Shared> files;
        MergedFileIndex::Shared index;
        EQ(true, db.getMergedFiles(*run, files, index));
        EQ(true, bool(index));
        EQ(RunInfo::RunNumber(1234), index->runNumber());
    }

    void fileEvents()
    {
        RFMDatabase db((m_tmp / "test.sqlite").string());
        RunInfo::Shared run;
        MergedFile::Shared file;
        FileEvent::Shared event;

        run = db.insertRun(1234, "EAR1", std::time(nullptr),
                           DaqInfo::CrateIdList());
        EQ(true, bool(run));

        file = db.insertMergedFile(*run, 42);
        EQ(true, bool(file));

        event = db.insertFileEvent(*file, 10, 12, 1024);
        EQ(true, bool(event));
        EQ(FileEvent::SequenceNumber(10), event->sequenceNumber());
        EQ(std::size_t(-1), event->offset());
        event->setOffset(12345);
        event->save();

        EQ(false, bool(db.insertFileEvent(*file, 10, 12, 123)));
        EQ(true, bool(db.insertFileEvent(*file, 11, 13, 124)));

        std::vector<FileEvent::Shared> events;
        std::size_t total;
        EQ(true, db.getFileEvents(*file, events, total));
        EQ(std::size_t(1148), total);
        EQ(std::size_t(2), events.size());
        EQ(FileEvent::SequenceNumber(11), events[1]->sequenceNumber());
        EQ(std::size_t(12345), events[0]->offset());
    }

    void nextExpiry()
    {
        RFMDatabase db((m_tmp / "test.sqlite").string());
        RunInfo::Shared run;
        std::time_t now = std::time(nullptr);

        run = db.insertRun(1234, "EAR1", now, DaqInfo::CrateIdList());
        EQ(true, bool(run));
        run->setExpiryDate(now);
        run->save();

        run = db.insertRun(1235, "EAR1", now, DaqInfo::CrateIdList());
        EQ(true, bool(run));
        run->setExpiryDate(now - 5);
        run->save();

        RunInfo::RunNumber runNumber;
        std::time_t next;
        EQ(true, db.getNextExpiry(runNumber, next));
        EQ(RunInfo::RunNumber(1235), runNumber);
        EQ(now - 5, next);

        run->setExpiryDate(RunInfo::NoExpiry);
        run->save();

        EQ(true, db.getNextExpiry(runNumber, next));
        EQ(RunInfo::RunNumber(1234), runNumber);
        EQ(now, next);

        run = db.getRun(1234);
        EQ(true, bool(run));
        run->setExpiryDate(RunInfo::Expired);
        run->save();

        EQ(true, db.getNextExpiry(runNumber, next));
        EQ(RunInfo::InvalidRunNumber, runNumber);
        EQ(RunInfo::NoExpiry, next);
    }

    void nextRetry()
    {
        RFMDatabase db((m_tmp / "test.sqlite").string());
        std::time_t now = std::time(nullptr);
        RunInfo::Shared run;
        MergedFile::Shared file;

        run = db.insertRun(1234, "EAR1", now, DaqInfo::CrateIdList());
        EQ(true, bool(run));

        file = db.insertMergedFile(*run, 42);
        EQ(true, bool(file));

        RunInfo::RunNumber runNumber;
        MergedFile::FileNumber fileNumber;
        std::time_t date;
        EQ(true, db.getNextRetry(runNumber, fileNumber, date));
        EQ(RunInfo::InvalidRunNumber, runNumber);
        EQ(MergedFile::InvalidFileNumber, fileNumber);
        EQ(std::time_t(-1), date);

        file->setStatus(MergedFile::FAILED);
        EQ(uint32_t(1), file->retries());
        file->save();

        run.reset();
        file.reset();

        EQ(true, db.getNextRetry(runNumber, fileNumber, date));
        EQ(RunInfo::RunNumber(1234), runNumber);
        EQ(MergedFile::FileNumber(42), fileNumber);
        DBL_EQ(now + (1 << 0), date, 5);

        run = db.getRun(1234);
        EQ(true, bool(run));
        file = run->currentFile();
        EQ(uint32_t(1), file->retries());
        for (int i = 0; i < 10; ++i)
        {
            file->setStatus(MergedFile::WAITING);
            file->setStatus(MergedFile::FAILED);
            file->save();
        }
        EQ(uint32_t(11), file->retries());

        EQ(true, db.getNextRetry(runNumber, fileNumber, date));
        EQ(RunInfo::RunNumber(1234), runNumber);
        EQ(MergedFile::FileNumber(42), fileNumber);
        DBL_EQ(now + (1 << 10), date, 5);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestRFMDatabase);
